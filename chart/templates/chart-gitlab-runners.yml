{{- if not (empty .Values.runners) }}
---
kind: Namespace
apiVersion: v1
metadata:
  name: gitlab
  labels: {{ include "common.labels.standard" . | nindent 4}}
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: {{ include "common.names.fullname" . }}
  labels: {{ include "common.labels.standard" . | nindent 4}}
subjects:
- kind: User
  name: system:serviceaccount:gitlab:default
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
{{- if .Values.persistence.enabled }}
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ include "common.names.fullname" . }}
  namespace: gitlab
  labels: {{ include "common.labels.standard" . | nindent 4}}
spec:
  storageClassName: {{ .Values.persistence.storageClass | default "-" }}
  accessModes:
    - {{ .Values.persistence.accessMode | default "ReadWriteMany" }}
  resources:
     requests:
       storage: {{ .Values.persistence.size | default "25Gi" }}
{{- end }}
{{ range $index, $runner := .Values.runners }}
---
apiVersion: v1
kind: Secret
metadata:
  namespace: gitlab
  name: {{ include "common.names.fullname" $ }}-{{ $index }}-gitlab-runner-secret
  labels: {{ include "common.labels.standard" $ | nindent 4}}
type: Opaque
data:
  #base64 encoded registration token
  runner-registration-token: {{ index $runner "registration-token" | default "" | b64enc | quote }}
  runner-token: {{ index $runner "token" | default "" | b64enc | quote }}
---
apiVersion: helm.cattle.io/v1
kind: HelmChart
metadata:
  name: gitlab-runner-{{ $index }}
  namespace: kube-system
  labels: {{ include "common.labels.standard" $ | nindent 4}}
spec:
  repo: https://charts.gitlab.io
  chart: gitlab-runner
  targetNamespace: gitlab
---
apiVersion: helm.cattle.io/v1
kind: HelmChartConfig
metadata:
  name: gitlab-runner-{{ $index }}
  namespace: kube-system
  labels: {{ include "common.labels.standard" $ | nindent 4}}
spec:
  valuesContent: |-
    nodeSelector:
      kubernetes.io/os: linux
    sessionServer: 
      enabled: false
    concurrent: 24
    checkInterval: 0
    runners:
      privileged: true
      secret: {{ include "common.names.fullname" $ }}-{{ $index }}-gitlab-runner-secret
      config: |
        # [session_server]
        #   listen_address = "[::]:8093"
        #   advertise_address = "remote.xrow.de:8093"
        #   session_timeout = 1800

        [[runners]]
            name = "gitlab-runner-{{ $index }}-{{ $runner.name }}"
            url = "https://gitlab.com"
            token = "{{ $runner.token }}"
            output_limit = 100000
            environment = ["TZ=Europe/Berlin"]
            cache_dir = "/cache"
            executor = "kubernetes"
            # https://docs.gitlab.com/runner/executors/kubernetes.html
            [runners.kubernetes]
                image = "registry.gitlab.com/xrow-public/ci-tools/tools:main"
                privileged = true
                pull_policy = ["always"]
                cpu_request = "200m"
                cpu_limit = "1000m"
                namespace = "gitlab"
                [runners.kubernetes.node_selector]
                  "kubernetes.io/os" = "linux"
                [runners.kubernetes.node_tolerations]
                  "node-role.kubernetes.io/master" = "NoSchedule"
                [runners.kubernetes.volumes]
                  [[runners.kubernetes.volumes.pvc]]
                    name = "{{ include "common.names.fullname" $ }}"
                    mount_path = "/cache"
    rbac:
      create: false
      serviceAccountName: default
    gitlabUrl: https://gitlab.com
    runnerRegistrationToken:
{{- if $.Values.persistence.enabled }}
    volumeMounts:
    - name: cache
      mountPath: /cache
    volumes:
    - name: cache
      persistentVolumeClaim:
        claimName: {{ include "common.names.fullname" $ }}
{{ end }}
---
{{ end }}
{{ end }}