# Documentation

## Run local

```bash
podman run -it -v "$(pwd):/workdir:Z" registry.gitlab.com/xrow-public/ci-tools/mkdocs:main bash -c "source <(curl -s https://gitlab.com/xrow-public/ci-tools/-/raw/main/scripts/library.sh) && ci_mkdocs_build docs"
```
