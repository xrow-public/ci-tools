---
description: The Ibexa Digital Experience Platform Helm Chart and Operator is a cloud native software-based smart agent build up on HELM and the Operator Framework and SDK that handles administration of Ibexa DXP.
---


# Ibexa DXP Helm Chart and Operator for Kubernetes and Cloud
