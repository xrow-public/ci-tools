spec:
  inputs:
    configuration:
      description: Points to a Helm Chart configuration file in the deploy/ directory of the Ibexa application.
      default: ""
    release:
      description: The name of the Helm chart release.
    chart:
      description: "The URL of the Helm Chart in an OCI registry (that is, beginning with oci://)"
      default: "oci://registry.gitlab.com/xrow-shared/helm-ezplatform/charts/ibexa"
    version:
      description: The version of the chart to deploy. Pick 0.0.0=latest, 0.0.0+CI_COMMIT_SHA, current=tag or hash.
      default: "0.0.0"
    context:
      description: The Kubernetes context that the Helm Chart will deploy to.
    namespace:
      description: The Kubernetes namespace that the Helm Chart will deploy to.
    type:
      description: The type of deployment to perform.
      default: "deploy"
      options: ['deploy', 'setup']
    action:
      description: Control whether the deployment job should always run or only when started manually.
      default: "manual"
      options: ['manual', 'always']
    action-condition:
      description: The condition that must be met for the job to run.
      default: "$CI_COMMIT_REF_SLUG =~ $CI_PATTERN_TRUNK || $CI_COMMIT_REF_SLUG =~ $CI_PATTERN_STABLE_BRANCH || $CI_COMMIT_TAG =~ $CI_PATTERN_SEMVER"
    timeout:
      description: Timeout for the deployment job.
      default: 20m
    url:
      default: "test.dev.example.com"
    stage:
      default: "deploy"
    tag:
      default: ""
    needs:
      description: The jobs that this job depends on.
      default: []
      type: array
---

# Deployment Step 1: Validate the Helm Chart and its configuration
helm:lint:$[[ inputs.configuration ]]:
  image: $CI_IMAGE_TOOLS
  stage: lint
  tags: ['$[[ inputs.tag ]]']
  variables:
    HELM_RELEASE: "$[[ inputs.release ]]"
    CI_ENVIRONMENT_HOST: "$[[ inputs.url | expand_vars ]]"
  dependencies: []
  rules:
    - exists:
        - deploy/$[[ inputs.configuration ]].yaml
  script:
    - |
      source <(curl -s https://$CI_SERVER_HOST/xrow-public/ci-tools/-/raw/main/scripts/library.sh)
      ci_helm_get_chart $[[ inputs.chart ]]:$[[ inputs.version ]]
      ci_helm_lint_config $[[ inputs.configuration ]]
  rules:
    - when: on_success

# Deployment Step 2: Deploy the Helm Chart
$[[ inputs.type ]]:$[[ inputs.context ]]:$[[ inputs.namespace ]]:$[[ inputs.release ]]:
  timeout: $[[ inputs.timeout ]]
  image: $CI_IMAGE_TOOLS
  needs:
    - $[[ inputs.needs ]]
    - "helm:lint:$[[ inputs.configuration ]]"
  variables:
    HELM_RELEASE: "$[[ inputs.release ]]"
    CI_ENVIRONMENT_HOST: "$[[ inputs.url | expand_vars ]]"
  dependencies: []
  script:
  - |
    source <(curl -s https://$CI_SERVER_HOST/xrow-public/ci-tools/-/raw/main/scripts/library.sh)
    ci_helm_get_chart $[[ inputs.chart ]]:$[[ inputs.version ]]
    ci_kubernetes_config $[[ inputs.context ]]
    ci_kubernetes_create_namespace $[[ inputs.namespace ]]
    if [ "$[[ inputs.type ]]" == "setup" ]; then
      ci_helm_deploy_chart "$[[ inputs.release ]]" "deploy/$[[ inputs.configuration ]].yaml" "true"
    else
      ci_helm_deploy_chart "$[[ inputs.release ]]" deploy/$[[ inputs.configuration ]].yaml
    fi
  resource_group: "$[[ inputs.context ]]/$[[ inputs.namespace ]]"
  stage: $[[ inputs.stage ]]
  tags: ['$[[ inputs.tag ]]']
  environment:
    name: $[[ inputs.context ]]/$[[ inputs.namespace ]]
    url: https://$[[ inputs.url ]]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '"$[[ inputs.action ]]" == "always" && ( $[[ inputs.action-condition ]] )'
      when: on_success
    - if: '"$[[ inputs.action ]]" == "manual" && ( $[[ inputs.action-condition ]] )'
      when: manual
    - when: never