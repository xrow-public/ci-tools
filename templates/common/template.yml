spec:
  inputs:
    version:
      default: "main"
      type: string
    stages:
      description: The stages available in the pipeline.
      default:
        - lint 
        - build
        - test
        - deploy
      type: array
    flag-fastestmirror:
      default: "false"
      type: string
    repo:
      default: "registry.gitlab.com/xrow-public/ci-tools"
      type: string
    image-trivy:
      default: docker.io/aquasec/trivy:latest
      description: The image to use for trivy.
      type: string
    image-podman:
      default: ${CI_TOOLS_REPO}/podman:${CI_TOOLS_VERSION}
      description: The image to use for podman.
      type: string
    image-ansible:
      default: ${CI_TOOLS_REPO}/ansible:${CI_TOOLS_VERSION}
      description: The image to use for ansible.
      type: string
    image-ansible-ee:
      default: ${CI_TOOLS_REPO}/ansible-ee:${CI_TOOLS_VERSION}
      description: The image to use for ansible.
      type: string
    image-tools:
      default: ${CI_TOOLS_REPO}/tools:${CI_TOOLS_VERSION}
      description: The image to use for tools.
      type: string
    image-lint:
      default: ${CI_TOOLS_REPO}/lint:${CI_TOOLS_VERSION}
      description: The image to use for lint.
      type: string
    image-mkdocs:
      default: ${CI_TOOLS_REPO}/mkdocs:${CI_TOOLS_VERSION}
      description: The image to use for mkdocs.
      type: string
    image-helm-docs:
      default: ${CI_TOOLS_REPO}/helm-docs:${CI_TOOLS_VERSION}
      description: The image to use for helmdocs.
      type: string
    image-semantic-release:
      default: ${CI_TOOLS_REPO}/semantic-release:${CI_TOOLS_VERSION}
      description: The image to use for semantic-release.
      type: string
    image-renovate:
      default: ${CI_TOOLS_REPO}/renovate:${CI_TOOLS_VERSION}
      description: The image to use for semantic-release.
      type: string
    image-spellcheck:
      default: ${CI_TOOLS_REPO}/spellcheck:${CI_TOOLS_VERSION}
      description: The image to use for spellcheck.
      type: string
    registry:
      description: Registry to store container image releases in
      default: "$CI_REGISTRY"
    repository-path:
      description: Repository to store container image releases in
      default: "$CI_PROJECT_PATH"
---

variables:
  CI_REGISTRY: "$[[ inputs.registry | expand_vars ]]"
  CI_REGISTRY_PROJECT_PATH: "$[[ inputs.repository-path | expand_vars ]]"
  GIT_STRATEGY: fetch
  GIT_DEPTH: 3
  CI_PATTERN_TRUNK: '/^(master|main)$/'
  CI_PATTERN_STABLE_BRANCH: '/^(0|[1-9]\d*)\.(0|[1-9]\d*)$/'
  # https://semver.org/#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string
  CI_PATTERN_SEMVER: '/^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/'
  FF_GITLAB_REGISTRY_HELPER_IMAGE: 1
  FF_ENABLE_BASH_EXIT_CODE_CHECK: "true"
  CI_TOOLS_VERSION: "$[[ inputs.version ]]"
  CI_TOOLS_REPO: "$[[ inputs.repo ]]"
  CI_CONFIG_FLAG_FASTESTMIRROR: "$[[ inputs.flag-fastestmirror ]]"
  CI_IMAGE_TOOLS: "$[[ inputs.image-tools | expand_vars ]]"
  CI_IMAGE_LINT: "$[[ inputs.image-lint | expand_vars ]]"
  CI_IMAGE_MKDOCS: "$[[ inputs.image-mkdocs | expand_vars ]]"
  CI_IMAGE_PODMAN: "$[[ inputs.image-podman | expand_vars ]]"
  CI_IMAGE_TRIVY: "$[[ inputs.image-trivy | expand_vars ]]"
  CI_IMAGE_ANSIBLE: "$[[ inputs.image-ansible | expand_vars ]]"
  CI_IMAGE_ANSIBLE_EE: "$[[ inputs.image-ansible-ee | expand_vars ]]"
  CI_IMAGE_SEMANTIC_RELEASE: "$[[ inputs.image-semantic-release | expand_vars ]]"
  CI_IMAGE_RENOVATE: "$[[ inputs.image-renovate | expand_vars ]]" 
  CI_IMAGE_SPELLCHECK: "$[[ inputs.image-spellcheck | expand_vars ]]"
  CI_IMAGE_HELMDOCS: "$[[ inputs.image-helm-docs | expand_vars ]]" 
  CI_DEBUG_TRACE:
    description: "Debug mode is disabled by default. Debug mode exposes security tokens. Set 'CI_DEBUG_TRACE=false' to disable"
    value: "false"
    options:
      - "false"
      - "true"

stages: $[[ inputs.stages ]]
