spec:
  inputs:
    name: 
      default: container
    type:
      default: php
    path:
      description: Path to the directory containing Container sources / context.
      default: .
    containerfile-path:
      description: Alternate path to the Containerfile, if either Dockerfile or Containerfile in the conect directory.
      default: ""
    args:
      default: ""
    code-quality:
      # @TODO bug in gitlab
      # type: boolean
      type: string
      default: "true"
    composer-audit-abandoned:
      description: How should the security:php job handle abandoned composer packages (overrides settings in composer.json)?
      options: [ 'ignore', 'report', 'fail' ]
      default: fail
    image:
      description: Base container image
      default: registry.access.redhat.com/ubi9/php-81:1-45
    test-translation:
      description: Enable or disable the test:translation job
      options: [ 'enable', 'disable' ]
      default: "enable"
---
# https://$CI_SERVER_HOST/gitlab-org/gitlab/-/issues/440643
# @TODO 
# test-fail:
#    script: exit 1
#    allow_failure: $[[ inputs.allow_failure ]]
# instead of CODE_QUALITY_DISABLED

variables:
  CODE_QUALITY_ENABLED: $[[ inputs.code-quality ]]
  TEST_TRANSLATION: $[[ inputs.test-translation ]]

include:
- component: $CI_SERVER_HOST/xrow-public/ci-tools/lint-json@main
- component: $CI_SERVER_HOST/xrow-public/ci-tools/lint-javascript@main
- component: $CI_SERVER_HOST/xrow-public/ci-tools/lint-markdown@main
- component: $CI_SERVER_HOST/xrow-public/ci-tools/lint-yaml@main
- component: $CI_SERVER_HOST/xrow-public/ci-tools/lint-style@main
- component: $CI_SERVER_HOST/xrow-public/ci-tools/security-npm@main
  inputs:
    name: $[[ inputs.name ]]

- component: $CI_SERVER_HOST/xrow-public/ci-tools/container@main
  inputs:
    name: $[[ inputs.name ]]
    path: $[[ inputs.path ]]
    args: COMPOSER_VERSION COMPOSER_AUTH COMPOSER_ARGS GIT_SSH_KEY CI_JOB_TOKEN GITLAB_TOKEN S2I_IMAGE COMPOSER_PROCESS_TIMEOUT $[[ inputs.args ]]

build:$[[ inputs.name ]]:
  variables:
    S2I_IMAGE: $[[ inputs.image ]]
  cache:
    key:
      files:
      - composer.json
      - package.json
    paths:
    - vendor
    - node_modules
  allow_failure: false
  script:
    - |
      source <(curl -s https://$CI_SERVER_HOST/xrow-public/ci-tools/-/raw/main/scripts/library.sh)
      ci_s2i_container_file $[[ inputs.path ]] $[[ inputs.type ]]
      ci_container_build $[[ inputs.path ]] $[[ inputs.containerfile-path ]]
      ci_container_push_build 
  rules:
  - if: '$CI_PIPELINE_SOURCE == "schedule"'
    when: never
  - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    when: on_success
  - when: on_success
  artifacts:
    when: always
    paths:
      - release/s2i-build.log

# @TODO clean env from build vars,
# --runtime-image $S2I_IMAGE --runtime-artifact /opt/app-root/src
# This can`t help since env is the same. https://github.com/openshift/source-to-image/blob/master/docs/runtime_image.md

security:php:$[[ inputs.name ]]:
  variables:
    COMPOSER_AUDIT_ABANDONED: $[[ inputs.composer-audit-abandoned ]]
  stage: test
  image:
    name: $CI_REGISTRY_IMAGE/$[[ inputs.name ]]:$CI_COMMIT_SHA
    entrypoint: ["/bin/bash", "-c"]
  needs:
    - job: "build:$[[ inputs.name ]]"
      artifacts: false
  script:
    - |
      echo "Change dir to ${APP_DATA}" 
      cd ${APP_DATA}
      if [ -f composer.lock ]; then
        composer --version
        echo "COMPOSER_AUDIT_ABANDONED = '${COMPOSER_AUDIT_ABANDONED}'"
        composer audit
      else
        echo 'composer.lock not found.';
        exit 0;
      fi
  rules:
    - if: '$CODE_QUALITY_ENABLED == "true"'
      allow_failure: true
      exists:
        - composer.json
      when: on_success
    - exists:
        - composer.json
      when: on_success

lint:twig:
  stage: test
  image:
    # Use the built project image because Twig templates usually rely on
    # extensions defined in vendor packages, and the linting tool is provided
    # by a Symfony package.
    name: $CI_REGISTRY_IMAGE/$[[ inputs.name ]]:$CI_COMMIT_SHA
    entrypoint: ["/bin/bash", "-c"]
  dependencies: []
  script:
    - |
      cd ${APP_DATA}
      php bin/console lint:twig templates/ || error_code=$?
      if [[ "${error_code}" -eq 65 ]]; then
        echo "Missing translations.";
        exit 1;
      elif [[ "${error_code}" > 0 ]]; then
        exit 1;
      fi
      php bin/console lint:twig src || error_code=$?
      if [[ "${error_code}" -eq 65 ]]; then
        echo "Missing translations.";
        exit 1;
      elif [[ "${error_code}" > 0 ]]; then
        exit 1;
      fi
  rules:
    - if: '$CODE_QUALITY_ENABLED == "true"'
      allow_failure: true
      exists:
        - templates/**/*.twig
        - src/**/*.twig
      when: on_success
    - exists:
        - templates/**/*.twig
        - src/**/*.twig
      when: on_success

lint:composer:
  image:
    name: registry.gitlab.com/xrow-public/ci-tools/symfony:main
    entrypoint: ["/bin/bash", "-c"]
  stage: lint
  script:
    - |
      source <(curl -s https://$CI_SERVER_HOST/xrow-public/ci-tools/-/raw/main/scripts/library.sh)
      ci_composer_init
      # Don't use '--strict'; ignore warnings because Ibexa always has warnings about exact version constraints.
      $COMPOSER_EXECUTABLE validate --with-dependencies --no-check-publish
  rules:
    - exists:
      - composer.json

test:phpunit:
  stage: test
  image:
    name: $CI_REGISTRY_IMAGE/$[[ inputs.name ]]:$CI_COMMIT_SHA
    entrypoint: ["/bin/bash", "-c"]
  needs:
    - job: "build:$[[ inputs.name ]]"
      artifacts: false
  script:
    - |
      echo "Change dir to ${APP_DATA}" 
      cd ${APP_DATA}
      if [ -f phpunit.xml ]; then
        vendor/bin/phpunit --configuration phpunit.xml;
      else
        echo 'phpunit.xml not found.';
        exit 0;
      fi
  rules:
    - if: '$CODE_QUALITY_ENABLED == "true"'
      allow_failure: true
      exists:
        - phpunit.xml
    - exists:
        - phpunit.xml

test:php:codestyle:
  image:
    name: registry.access.redhat.com/ubi9/php-81
    entrypoint: ["/bin/bash", "-c"]
  needs: []
  dependencies: []
  script:
    - |
      source <(curl -s https://$CI_SERVER_HOST/xrow-public/ci-tools/-/raw/main/scripts/library.sh)
      test_php_codestyle
  rules:
    - if: '$CODE_QUALITY_ENABLED == "true"'
      allow_failure: true
      exists:
        - "**/*.php"
    - exists:
        - "**/*.php"

test:php:rector:
  stage: test
  image:
    name: rector/rector:latest
    entrypoint: ["/bin/sh", "-c"]
  script:
    - /rector/bin/rector process . --config ./rector.yaml --set symfony50
  rules:
    - if: '$CODE_QUALITY_ENABLED == "true"'
      allow_failure: true
      exists:
        - rector.yaml
    - exists:
        - rector.yaml

test:php:loc:
  stage: test
  image:
    name: registry.gitlab.com/xrow-public/ci-tools/symfony:main
    entrypoint: ["/bin/bash", "-c"]
  allow_failure: true
  needs: []
  script:
    - cd ${CI_PROJECT_DIR}
    - phploc $[[ inputs.path ]]/src/
  dependencies: []
  rules:
    - if: '$CODE_QUALITY_ENABLED == "true" && ($CI_COMMIT_TAG || $CI_COMMIT_BRANCH)'
      allow_failure: true
      exists:
        - $[[ inputs.path ]]/**/*.php
    - if: "$CI_COMMIT_TAG || $CI_COMMIT_BRANCH"
      exists:
        - $[[ inputs.path ]]/**/*.php

test:php:copypastedetection:
  stage: test
  image:
    name: registry.access.redhat.com/ubi9/php-80
    entrypoint: ["/bin/bash", "-c"]
  needs: []
  script:
    - source <(curl -s https://$CI_SERVER_HOST/xrow-public/ci-tools/-/raw/main/scripts/library.sh)
    - test_phpcpd
  dependencies: []
  rules:
    - if: '$CODE_QUALITY_ENABLED == "true" && ($CI_COMMIT_TAG || $CI_COMMIT_BRANCH)'
      allow_failure: true
      exists:
        - "**/*.php"
        - "**/*.twig"
        - "**/*.scss"
        - "**/*.css"
    - if: "$CI_COMMIT_TAG || $CI_COMMIT_BRANCH"
      exists:
        - "**/*.php"
        - "**/*.twig"
        - "**/*.scss"
        - "**/*.css"

test:php:messdetection:
  stage: test
  image:
    name: registry.access.redhat.com/ubi9/php-80
    entrypoint: ["/bin/bash", "-c"]
  needs: []
  script:
    - source <(curl -s https://$CI_SERVER_HOST/xrow-public/ci-tools/-/raw/main/scripts/library.sh)
    - test_phpmd
  dependencies: []
  rules:
    - if: '$CODE_QUALITY_ENABLED == "true" && ($CI_COMMIT_TAG || $CI_COMMIT_BRANCH)'
      allow_failure: true
      exists:
        - "**/*.php"
    - if: "$CI_COMMIT_TAG || $CI_COMMIT_BRANCH"
      allow_failure: true
      exists:
        - "**/*.php"

test:php:codequality:
  stage: test
  image:
    name: $CI_REGISTRY_IMAGE/$[[ inputs.name ]]:$CI_COMMIT_SHA
    entrypoint: ["/bin/bash", "-c"]
  needs:
    - job: "build:$[[ inputs.name ]]"
      artifacts: false
  script:
    - |
      echo "Change dir to ${APP_DATA}" 
      cd ${APP_DATA}
      source <(curl -s https://$CI_SERVER_HOST/xrow-public/ci-tools/-/raw/main/scripts/library.sh)
      test_php_codequality
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
  rules:
    - if: '$CODE_QUALITY_ENABLED == "true" && ( $CI_PIPELINE_SOURCE == "schedule" || $CI_MERGE_REQUEST_ID || $CI_COMMIT_REF_SLUG =~ $CI_PATTERN_TRUNK || $CI_COMMIT_TAG =~ $CI_PATTERN_SEMVER )'
      allow_failure: true
      exists:
        - tests/bootstrap.php
    - if: '$CI_PIPELINE_SOURCE == "schedule" || $CI_MERGE_REQUEST_ID || $CI_COMMIT_REF_SLUG =~ $CI_PATTERN_TRUNK || $CI_COMMIT_TAG =~ $CI_PATTERN_SEMVER'
      exists:
        - tests/bootstrap.php

test:translation:
  stage: test
  image:
    name: $CI_REGISTRY_IMAGE/$[[ inputs.name ]]:$CI_COMMIT_SHA
    entrypoint: ["/bin/bash", "-c"]
  needs:
    - job: "build:$[[ inputs.name ]]"
      artifacts: false
  script:
    - |
      echo "Change dir to ${APP_DATA}" 
      cd ${APP_DATA}
      echo "@TODO Bug in https://github.com/symfony/symfony/issues/47562"
      source <(curl -s https://$CI_SERVER_HOST/xrow-public/ci-tools/-/raw/main/scripts/library.sh)
      test_symfony_translation
  allow_failure: true
  rules:
    - if: $TEST_TRANSLATION == "disable"
      when: never
    - if: '$CODE_QUALITY_ENABLED == "true" && ( $CI_PIPELINE_SOURCE == "schedule" || $CI_MERGE_REQUEST_ID || $CI_COMMIT_REF_SLUG =~ $CI_PATTERN_TRUNK || $CI_COMMIT_TAG =~ $CI_PATTERN_SEMVER )'
      allow_failure: true
      exists:
        - "**/*.twig"
        - "**/*.php"
        - "**/*.xlf"
        - "**/*.en.yml"
    - if: '$CI_PIPELINE_SOURCE == "schedule" || $CI_MERGE_REQUEST_ID || ($CI_COMMIT_REF_SLUG =~ $CI_PATTERN_TRUNK || $CI_COMMIT_TAG =~ $CI_PATTERN_SEMVER )'
      exists:
        - "**/*.twig"
        - "**/*.php"
        - "**/*.xlf"
        - "**/*.en.yml"