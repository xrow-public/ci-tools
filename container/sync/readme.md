
```
podman kill -a
```

```
buildah rm --all && podman system prune --all --force && podman rmi --all
podman build . --force-rm -t test
podman run --rm -d --user root:root -p 2222:2222 -e "PASSWORD=password" test bash -c "sleep 200"
podman exec -it --user 1001:root -w /opt/app-root/src -e"HOME=/opt/app-root/src" -e "PASSWORD=password" ssh bash
podman run -it --user root:root --net=host -w /opt/app-root/src -e"HOME=/opt/app-root/src" -e "PASSWORD=password" test bash 
/usr/bin/container-entrypoint sleep 2
oc config use-context 06.xrow.net
oc port-forward pod/sync-files 2222:2222 --pod-running-timeout=1m0s -n web-cms-dev &
```

```
podman run -it --user 1002:1002 --tty --net=host --cap-add AUDIT_WRITE \
    -e "AUTHORIZED_KEY=ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPpq6ze1nQg+CA+N8i9sXH85KHONwtPUvoeG2RDaEUIm your_email@example.com" \
    test
```

```
podman run -it --user 0:0 --net=host --cap-add AUDIT_WRITE \
    -e "AUTHORIZED_KEY=ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOXGvbBWWfPKoMEU8zKMaKGLocGLVMAq4K24UZMyzjmc" \
    test
```

```
podman run -it --user 0:0 --net=host --cap-add AUDIT_WRITE \
    -e "AUTHORIZED_KEY=ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOXGvbBWWfPKoMEU8zKMaKGLocGLVMAq4K24UZMyzjmc" \
    test
```

```
podman run -it --user 0:0 --net=host --cap-add AUDIT_WRITE \
    -e "AUTHORIZED_KEY=ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOXGvbBWWfPKoMEU8zKMaKGLocGLVMAq4K24UZMyzjmc" \
    registry.gitlab.com/xrow-shared/helm-ezplatform/dev:3.3.11
# ok
```

```
podman run -it --user root:root --cap-add AUDIT_WRITE -e "PASSWORD=password" \
    -e "AUTHORIZED_KEY=ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOXGvbBWWfPKoMEU8zKMaKGLocGLVMAq4K24UZMyzjmc" \
    test
```

```
podman run -it --user 1001:root -e "PASSWORD=password" test bash
```

```
podman run -it --user root:root -e "PASSWORD=password" test bash
```
