#!/bin/bash

curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo
dnf -y install yarn
yarn global add gitlab-ci-validate
gitlab-ci-validate .gitlab-ci.yml