# CI/CD Pipeline Components

## Get started

Create a new project with 2 file.

A `Containerfile`:

```yaml
FROM alpine
```

A GitLab pipeline file `.gitlab-ci.yml`:

```yaml
include:
- component: $CI_SERVER_HOST/xrow-public/ci-tools/common@main
- component: $CI_SERVER_HOST/xrow-public/ci-tools/container@main
  inputs:
    name: myapplication
    path: .
```

Now look the results from the pipeline builds. A new container has been pushed to the GitLab registry.

## How components are used?

All `components` are in the folder `templates`. Each individual pipeline reuses some parts of the components. Components are used with the GitLab `include` keyword as documented in the GitLab Component documentation. 

Example:

```yaml
include:
- component: $CI_SERVER_HOST/xrow-public/ci-tools/common@main
- component: $CI_SERVER_HOST/xrow-public/ci-tools/container@main
  inputs:
    name: myapplication
    path: .
```

## Common variables

Some pipelines require [GitLab variables](https://docs.gitlab.com/ee/ci/variables/)

| Name | Default value | Required | Encoding | Description | Jobs related
| --- | --- | ---  | --- | ---  | ---
| CI_DEBUG_TRACE | unset | no | boolean | Enables debug mode verbose output and credentials | All pipelines
| KUBECONFIG | unset | yes | file or text(base64) | Credentials to the Kubernetes clusters | All jobs that interact with Kubernetes
| `GITLAB_TOKEN` | null | yes | text | Credentials with access to GitLab API for changelog access | All `mkdocs` jobs


## Usage of a pipeline

Include the `latest` pipeline like [s2i-php](https://gitlab.com/xrow-public/ci-tools/raw/main/templates/s2i/template.yml)

```yaml
include:
- component: $CI_SERVER_HOST/xrow-public/ci-tools/common@main
- component: $CI_SERVER_HOST/xrow-public/ci-tools/s2i@main
  inputs:
    name: myapplication
    type: php
    path: .
```

## Available Pipelines

| Name | Pipeline File | Example Project
| --- | --- | ---
| Generic S2I for HTML, JAVA, NODEJS, PHP, PYTHON, ... | [templates/s2i/template.yml](https://gitlab.com/xrow-public/ci-tools/raw/main/templates/s2i/template.yml) | ---
| Container | [templates/container/template.yml](https://gitlab.com/xrow-public/ci-tools/raw/main/templates/container/template.yml) | ---
| Helm | [templates/helm/template.yml](https://gitlab.com/xrow-public/ci-tools/raw/main/templates/container/template.yml) | ---
| Review | [templates/review/template.yml](https://gitlab.com/xrow-public/ci-tools/raw/main/templates/review/template.yml) | ---
| Cypress | [templates/cypress/template.yml](https://gitlab.com/xrow-public/ci-tools/raw/main/templates/cypress/template.yml) | ---
| PHP S2i | [templates/s2i-php/template.yml](https://gitlab.com/xrow-public/ci-tools/raw/main/templates/s2i-php/template.yml) | ---


## Minimal Pipeline Example of an application like Ibexa DXP

```yaml
TBD
```

## Review apps

Git Lab [Review apps](https://docs.gitlab.com/ee/ci/review_apps/) are currently available for all pipelines.

You can start a review app from a merge context as well as from any stable branch. There are two jobs that control the state of a review app. `review:start` and `review:stop`. To start a review press the `review:start` job in your pipeline. The testing instance will be available about 15 minutes later. You will also find a list of current running review apps and review URLs under `project->deployments->environments` in GitLab.

# Nesting components in new components

## Example for a new nodejs-18 component 

```yaml
spec:
  inputs:
    name: 
      default: myapplication
    image:
      default: registry.access.redhat.com/ubi9/nodejs-18:1-45
---
 
include:
- component: $CI_SERVER_HOST/xrow-public/ci-tools/common@main
- component: $CI_SERVER_HOST/xrow-public/ci-tools/s2i@main
  inputs:
    name: $[[ inputs.name ]]
    type: nodejs
    image: $[[ inputs.image ]]

# Continue with custom code
```
