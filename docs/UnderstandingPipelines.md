# Understanding Pipelines

## Common Stages

The following stages are implemented by default:

| Name | Description
| --- | ---
| .pre | default stage via GitLab
| lint | Linting stage for all linting jobs before build
| build | Build stage for all software builds
| test | Test stage to validate all software builds
| deploy | Deploy stage to ship a build to environments
| .post | default stage via GitLab populated with some helper tasks

## Example Overview of a S2I Pipeline

The pipeline will iterate through the stages in GitLab and hold on error, if it makes sense.

![Example Flow for a S2I Pipeline](images/s2i-pipeline.png "Example Flow for a S2I Pipeline").


## How a pipeline uses S2I Builds

A general documentation on S2I can be found [here](https://docs.openshift.com/container-platform/4.10/openshift_images/using_images/using-s21-images.html) or in specific [here for PHP](https://github.com/sclorg/s2i-php-container).

![S2I Build](images/s2i.png "S2I Build").

S2I Introduction Video

[![Introduction Video S2I](https://img.youtube.com/vi/flI6zx9wH6M/0.jpg)](https://www.youtube.com/watch?v=flI6zx9wH6M)


Within the Pipeline there is for each stack one build:container jobs and template that triggers this automation. See `.template:s2i:php_build` in `.gitlab-common.yml`