# Bash library for CI/CD

```bash
source <(curl -s -k https://gitlab.com/xrow-public/ci-tools/-/raw/main/scripts/library.sh) [version]
```

To load the library simply execute the command above. Replace `[version]` with either a branch name like `main` or a proper release version like `${CI_DOCUMENTATION_LATEST_TAG}`. Please see below:

```bash
source <(curl -s -k https://gitlab.com/xrow-public/ci-tools/-/raw/main/scripts/library.sh) ${CI_DOCUMENTATION_LATEST_TAG}
```

After that you can execute any function of the [available modules](https://gitlab.com/xrow-public/ci-tools/-/tree/main/scripts/lib), for example:

```bash
ci_helm_build_chart ./chart
```

## How jobs interact with the bash scripting library?

Some `components` delegate business logic to bash scripts available via a common library in `scripts/library.sh`. Each individual pipeline reuses some parts of the library. Components are loading the library as a remote script `source <(curl -s https://gitlab.com/xrow-public/ci-tools/-/raw/main/scripts/library.sh)`. `ci_lint_javascript` is a function out of the library `scripts/lib/lint.sh`. By convention the prefix of the function `lint` indicates use and location of the function.

Example from `templates/s2i-php/template.yml`:

```yaml
lint:javascript:
  script:
    - |
      source <(curl -s https://gitlab.com/xrow-public/ci-tools/-/raw/main/scripts/library.sh) main
      ci_lint_javascript
```