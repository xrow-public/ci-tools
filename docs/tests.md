# Available Tests

| Test Name | Test Type | Scope | Severity | Complexity | Language | Links | Status |
| --- | --- | --- | --- | --- | --- | --- | --- |
| <a name="lint:style">lint:style</a> | SCSS / CSS Linter | S2I Project | Required | Low | CSS | - | Broken |
| <a name="lint:bash">lint:bash</a> | Bash Linter | Any Project | Optional | Low | Bash | - | Works |
| <a name="lint:javascript">lint:javascript</a> | Linter | Any JavaScript Project | Required | Medium | - | - | Works |
| <a name="lint:config:helm">lint:config:helm</a> | Linter | Project with helm chart | Required | Low | YAML | - | Works |
| <a name="lint:documentation">lint:documentation</a> | Linter | Documentation | Required | Medium | Markdown | - | Works |
| <a name="test:operator">test:operator</a> | Test with Kind | Project with operator | Required | High | HELM | - | Works |
| <a name="test:helm">test:helm</a> | Test with Kind | Project with helm chart | Required | High | HELM | - | Works |
| <a name="test:cypress">test:cypress</a> | Test cypress e2e | Any | Optional | High | - | - | Works |
| <a name="test:php:security">test:php:security</a> | Security | Any PHP Project | Optional | Medium | - | - | Works |
| <a name="test:npm:security">test:npm:security</a> | Security | Any JavaScript Project | Optional | Medium | - | - | Works |
| <a name="test:translation">test:translation</a> | XLIFF Translation | Any Ibexa Project | Optional | Low | - | - | Works |
| <a name="test:php:messdetection">test:php:messdetection</a> | Quality Assurance | Project + Bundle | Optional | High | PHP | - | Works |
| <a name="test:php:copypastedetection">test:php:copypastedetection</a> | Quality Assurance | Any PHP Project | Optional | Low | - | - | Works |
