
# Trouble Shooting Guide

## Helm Deployment

If a helm deployment fails because of a timeout, it's useful to rerun the deployment and look at the logs of the problematic containers in the namespace or even the event logs. In most cases you will find there the cause for the failure.