# Introduction

This is a documentation about the pipeline repository.

The contents of this repository are:

* CI/CD library of GitLab Components
* A CI/CD Bash library

It is also available as a [PDF Download](https://xrow-public.gitlab.io/ci-tools/pdf/document.pdf)

Example pipeline:

![](./images/pipeline.png)
