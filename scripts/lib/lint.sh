#!/bin/bash

# Load and run shellcheck to analyse Bash scripts for bad code.
# See https://www.shellcheck.net
function ci_lint_bash() {
    RESULT=0
    yum install -y xz
    curl -s -L "https://github.com/koalaman/shellcheck/releases/download/stable/shellcheck-stable.linux.x86_64.tar.xz" | tar -xJ --strip-components=1 -C /usr/bin
    shellcheck --version

    for bashfile in $(find . -name "*.sh" -not -path "./.vscode-server/*" -not -path "./vendor/*" -not -path "./public/*" ); do
        echo "Linting bash: ${bashfile}";
        set +e
        shellcheck --severity=error ${bashfile}
        RETURN=$?
        set -e
        if [[ $RETURN == 0 ]] ; then 
          :
        else
          if [[ $RESULT == 0 ]] ; then 
            RESULT=1
          fi
        fi
    done

    if [ $RESULT == 0 ] ; then 
        echo "Test completed with success";
    else 
        echo "Test failed"; exit 1; 
    fi
}

# Load and run stylelint to lint all CSS and SCSS files.
# See https://www.npmjs.com/package/stylelint
function ci_lint_style() {
  npm install -g stylelint stylelint-config-standard stylelint-config-standard-scss @stylistic/stylelint-config postcss
  echo "Using stylelint version $(stylelint --version)"
  cat $TOOLS_DIR/scripts/config-root/.stylelintrc.json > .stylelintrc.json
  echo "Using stylelint configuration '.stylelintrc.json':"
  cat .stylelintrc.json

  set +e
  stylelint --config .stylelintrc.json --config-basedir=$(npm root -g) ./**/*.css ./**/*.scss --fix
  RETURN=$?;
  set -e

  if [[ $RETURN == 0 ]]; then
    echo 'Style is good.';
    return 0
  else
    echo 'Style is not good.';
    echo "Run 'stylelint --config .stylelintrc.json ./**/*.css ./**/*.scss --fix' to fix your code";
    return 1
  fi
}

# Load and run standard to lint all JS files.
# See https://www.npmjs.com/package/standard
function ci_lint_javascript() {
    npm install -g standard typescript eslint-plugin-react@latest @typescript-eslint/eslint-plugin@latest eslint-config-standard@latest eslint@^7.12.1 eslint-plugin-import@^2.22.1 eslint-plugin-node@^11.1.0 eslint-plugin-promise@^5.0.0 @typescript-eslint/parser@latest
    echo "Using standard version $(standard --version)"

    set +e
    # Modify package.json to exclude Ibexa's webpack config and pagebuilder loading
    # scripts from linting - they follow the Ibexa code style.
    # See https://www.npmjs.com/package/standard#how-do-i-ignore-files
    dnf -y install jq
    tmp=$(mktemp)
    jq '.standard.ignore += ["assets/js/react.blocks.js","webpack.config.js","ibexa.webpack.*"]' < package.json > "$tmp"
    cat $tmp > package.json

    # Create a dummy file to avoid "Error: All files matched by '.' are ignored." in case the project has no unexcluded JS files
    mktemp temp-XXXXXX.js
    if [ -d "src" ]; then
      standard --allow-empty-input ./src/**/*.js
    fi
    RETURN=$?;
    set -e

    if [[ $RETURN == 0 ]]; then
      echo 'Javascript code style is good.';
      return 0
    else
      echo 'Javascript code style is not good.';
      echo "Run 'standard --allow-empty-input ./src/**/*.js --fix' to fix your code";
      return 1
    fi
}


# Lint YAML files using yamllint
# See https://github.com/adrienverge/yamllint
function ci_lint_yaml() {
  dnf install -y yamllint
  echo "Using $(yamllint --version)"
  yamllint -c ${TOOLS_DIR}/scripts/config-root/.yamllint .
}
