#!/bin/bash

function ci_s2i_export_envs() {
  [[ ! -f $S2I_SRC_DIR/.s2i/environment ]] && return 0
  while IFS='=' read -r key temp || [ -n "$key" ]; do
    local isComment='^[[:space:]]*#'
    local isBlank='^[[:space:]]*$'
    [[ $key =~ $isComment ]] && continue
    [[ $key =~ $isBlank ]] && continue
    value=$(eval printf '%q' $temp)
    echo "ENV $key=$value" >> /tmp/Dockerfile.s2i;
  done < $S2I_SRC_DIR/.s2i/environment
}

function ci_s2i_container_file() {
  ci_util_init
  local dir="${1:-.}"
  local type="${2:-php}"

  if [[ ! -d $dir ]]; then
    echo "[error] Directory $dir does not exist. There are no sources to build."
    exit 1
  fi

  if [[ -f $dir/Containerfile ]]; then
    echo "[error] Found existing Containerfile"
    exit 1
  fi

  if [[ -f $TOOLS_DIR/scripts/files/components/s2i/$type/Containerfile ]]; then
    cat $TOOLS_DIR/scripts/files/components/s2i/$type/Containerfile | DOLLAR='$' envsubst >> $dir/Containerfile
  else
    echo "[info] No Containerfile found for $file. Using default"
    cat $TOOLS_DIR/scripts/files/components/s2i/common | DOLLAR='$' envsubst >> $dir/Containerfile
  fi
  echo "$(cat $dir/Containerfile)"

  # mkdir -p release

  # set -o pipefail
  # CONTAINER_BUILD_ARGS=$(echo "${params[@]}")
  # ci_container_build $dir | tee -a release/s2i-build.log
  # if [ "${PIPESTATUS[0]}" -ne "0" ]; then
  #   echo "S2I Build failed. Check the logs."
  #   exit 1
  # fi
  # if [[ ! -z "$(grep -E -i "app.ERROR:|ERROR:" release/s2i-build.log)" ]] ; then 
  #   echo "The build log has warnings or errors."; 
  #   exit 1; 
  # fi

}