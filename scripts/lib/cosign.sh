#!/bin/bash

function ci_cosign_install() {
  if [[ ! -x "$(command -v cosign)" ]]; then
    COSIGN_VERSION=$(curl -s https://api.github.com/repos/sigstore/cosign/releases/latest | grep tag_name | cut -d : -f2 | tr -d "v\", ") && \
    curl -sSfL -O "https://github.com/sigstore/cosign/releases/download/v${COSIGN_VERSION}/cosign-${COSIGN_VERSION}-1.x86_64.rpm" && \
    rpm -ivh cosign-${COSIGN_VERSION}-1.x86_64.rpm && \
    rm -Rf cosign-${COSIGN_VERSION}-1.x86_64.rpm
  fi
}
function ci_cosign_login() {
  if [[ -v CI_REGISTRY_PASSWORD && -v CI_REGISTRY_USER ]]; then
    echo "[info] Permission workaround for Bug https://$CI_SERVER_HOST/gitlab-org/gitlab/-/issues/444268"
    echo "${CI_REGISTRY_PASSWORD}" | cosign login ${CI_REGISTRY} --username=${CI_REGISTRY_USER} --password-stdin=true  > /dev/null || echo "[warn] Login to registry $CI_REGISTRY failed"
  else
    echo "${CI_JOB_TOKEN}" | cosign login ${CI_REGISTRY} --username=${CI_REGISTRY_USER} --password-stdin=true  > /dev/null || echo "[warn] Login to registry $CI_REGISTRY failed"
  fi

}
function ci_cosign() {
  local container=$( echo ${1} | sed s/\+/_/g )
  if [[ -f ${COSIGN_PRIVATE_KEY} ]] ; then
    ci_cosign_install
    ci_cosign_login
    rm -Rf import-cosign*
    cosign import-key-pair --key ${COSIGN_PRIVATE_KEY}
    cosign sign -y --key import-cosign.key ${container}
    echo "[info] Signed ${container}"
  else
    echo "[warn] ${container} is unsigned, because file \$COSIGN_PRIVATE_KEY is not set"
  fi
}