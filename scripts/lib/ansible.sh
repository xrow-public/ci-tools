#!/bin/bash

export ANSIBLE_CONFIG="${ANSIBLE_CONFIG:-ansible.cfg }"

function ansible_runner_init() {
  export PATH="${PATH}:${HOME}/.local/bin"
  export ANSIBLE_CONFIG="${ANSIBLE_CONFIG:-ansible.cfg}"
  dnf install -y python3.11 python3.11-pip
  python3.11 -m pip install pipx
  pipx install ansible-core
  pipx install ansible-runner
  chmod o-w .
  # pipx ensurepath need relogin
}

function ansible_init() {
  pipx ensurepath
  pipx install poetry
  poetry config virtualenvs.create false
  # if [ -v BUILD_CACHE_DIR ]; then
  #   echo "$(date +[%T];) Set ansible cache dir to $BUILD_CACHE_DIR/.ansible ..."
  #   export ANSIBLE_COLLECTIONS_PATHS=${BUILD_CACHE_DIR}/.ansible/collections:~/.ansible/collections:/usr/share/ansible/collections
  #   mkdir -p "${BUILD_CACHE_DIR}/.ansible/collections" || true
  #   export DEFAULT_ROLES_PATH=${BUILD_CACHE_DIR}/.ansible/roles:~/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles
  #   mkdir -p "${BUILD_CACHE_DIR}/.ansible/roles" || true
  # fi
  if [[ -f "requirements.txt" ]]; then 
    pip install -r requirements.txt
  else 
    echo "[info] requirements.txt not provided"; 
  fi
  if [[ -f "pyproject.toml" ]]; then 
    poetry install
  else 
    echo "[info] pyproject.toml not provided"; 
  fi
  if [[ -f "requirements.yml" ]]; then 
    ansible-galaxy install -r requirements.yml; 
    ansible-galaxy collection install -r requirements.yml; 
  else 
    echo "[info] requirements.yml not provided"; 
  fi
}
