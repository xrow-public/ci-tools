#!/bin/bash


function ci_operator_prepare() {
  local dir="${1:-.}"
  local chart="${2:-}"

  mkdir -p ${dir}/helm-charts || true
  if [[ ! -z $chart ]]; then
    local chart_version=$( ci_helm_get_chart_version $chart )
    local chart_name=$( ci_helm_get_chart_name $chart )
    TIMEOUT=" --timeout ${HELM_TIMEOUT} "; 
    echo "Pulling chart $CHART:$CHART_VERSION"
    helm pull oci://$chart_version --version $chart_name --untar -d ${dir}/helm-charts
  else
    for file in $(find release/charts/ -regextype posix-extended -iregex '.*[.]tgz' | sort -g); do
      tar -xf $file -C ${dir}/helm-charts
    done
  fi
  echo "FROM quay.io/operator-framework/helm-operator:v1.28.0" >> ${dir}/Containerfile
  echo "ENV HOME=/opt/helm" >> ${dir}/Containerfile
  echo "COPY helm-charts/ \${HOME}/helm-charts/" >> ${dir}/Containerfile
  echo "COPY watches.yaml \${HOME}/watches.yaml" >> ${dir}/Containerfile
  echo "WORKDIR \${HOME}" >> ${dir}/Containerfile
}
