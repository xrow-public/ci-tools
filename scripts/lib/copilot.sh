#!/bin/bash

function ci_copilot_install() {
  if [[ ! -x "$(command -v gh)" ]]; then
    dnf install 'dnf-command(config-manager)'
    dnf config-manager --add-repo https://cli.github.com/packages/rpm/gh-cli.repo
    dnf install gh
    gh extension install github/gh-copilot
  fi
}
