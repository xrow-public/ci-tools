#!/bin/bash

function ci_gitlab_set_cleanup_policy() {
  if [[ ! -v GITLAB_TOKEN ]]; then
    echo "[info] Skipping setting cleanup policy for registry. GITLAB_TOKEN is not set."
    return 0
  fi
  local branch
  branch="${CI_DEFAULT_BRANCH:-main}"
  echo "[info] Setting cleanup policy for tags"
  echo '{"container_expiration_policy_attributes":{"cadence":"7d","enabled":true,"keep_n":100,"older_than":"14d","name_regex":".*","name_regex_keep":"(?:[0-9]+.[0-9]+.[0-9]+|'"${branch}"'|latest)"}}' > /tmp/request.tmp
  (
    curl --fail-with-body --request PUT -s "https://$CI_SERVER_HOST/api/v4/projects/${CI_PROJECT_ID}" \
    --header "Content-Type: application/json;charset=UTF-8" \
    --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
    --data @/tmp/request.tmp
  )
  return 0
}

function ci_gitlab_semantic_versioning() {
  ci_gitlab_set_cleanup_policy

  if [[ -z "$CI_PROJECT_DESCRIPTION" ]]; then
    echo "Project must have a description. In order to use semantic versioning."
    return 1
  fi
  export CI_CHANGELOG="${1:-CHANGELOG.md}"
  export SEMANTIC_RELEASE_OPTS="${SEMANTIC_RELEASE_OPTS:---debug}"
  if [[ ! -f ".releaserc.json" ]]; then
    echo "[info] Creating .releaserc.json file"
    cp $TOOLS_DIR/scripts/files/components/semantic-release/.releaserc.json .releaserc.json
    ci_envsubst .releaserc.json
  fi
  if [[ ! -x "$(command -v semantic-release)" ]]; then
    npm install -g semantic-release \
                  semantic-release/commit-analyzer \
                  semantic-release/git \
                  semantic-release/gitlab \
                  semantic-release/gitlab-config \
                  semantic-release/release-notes-generator \
                  semantic-release/exec \
                  semantic-release/changelog \
                  conventional-changelog-conventionalcommits
  fi
  semantic-release "${SEMANTIC_RELEASE_OPTS}"
}