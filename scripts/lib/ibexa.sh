#!/bin/bash

export COMPOSER_FUND="0"

# Configure composer, NPM and Yarn cache directories.
ci_ibexa_init_build_cache() {
  echo "$(date +[%T];) Set cache dirs ..."

  if [ -v BUILD_CACHE_DIR ]; then
    echo "$(date +[%T];) Set cache dir to $BUILD_CACHE_DIR..."
    echo "Permissions of $BUILD_CACHE_DIR"
    ls -lda $BUILD_CACHE_DIR
    export COMPOSER_CACHE_DIR=${BUILD_CACHE_DIR}/.composer
    mkdir -p $COMPOSER_CACHE_DIR || true

    # Must exist on Host System and must be 777
    export NPM_CONFIG_PREFIX=${BUILD_CACHE_DIR}/.npm-global
    export YARN_CACHE_FOLDER=${BUILD_CACHE_DIR}/.yarn
    mkdir -p $NPM_CONFIG_PREFIX || true
    mkdir -p $YARN_CACHE_FOLDER || true
  else
    echo "$(date +[%T];) Environment variable BUILD_CACHE_DIR is not set ..."
  fi
}

ci_ibexa_build_from_bundle() {
    ci_composer_init
    export COMPOSER_MIRROR_PATH_REPOS="1"
    local dir
    if [[ ! -v 1 ]]; then
      dir=$(mktemp -d -p /tmp)
    else
      dir="$1"
    fi;

    export ibexa_version="${3:-4.6.0}"
    export ibexa_package="${2:-oss}"

    mkdir -p $dir || true
    echo "Install dir: $dir"

    ci_ibexa_build $dir ibexa/${ibexa_package}-skeleton $ibexa_version
    bundle_name="$(cat composer.json | jq --raw-output '.["name"]')"

    echo "$(date +[%T];) Composer: Add local repository from $(pwd)"
    $COMPOSER_EXECUTABLE config --working-dir=$dir repositories.test "{\"type\": \"vcs\", \"url\": \"$(pwd)\"}"

    if [[ -v CI_COMMIT_BRANCH ]]; then
      branch="$CI_COMMIT_BRANCH"
    else
      branch="$CI_DEFAULT_BRANCH"
    fi

    git fetch -a

    echo "[info] Fix Branch: git checkout -b \"$branch\" \"$CI_COMMIT_SHA\""
    git checkout -b "$branch" "$CI_COMMIT_SHA"

    if [[ -v GITLAB_TOKEN ]]; then
      echo "$(date +[%T];) Composer: Add shared repository repositories.gitlab.com/612647"
      $COMPOSER_EXECUTABLE config --working-dir=$dir repositories.gitlab.com/612647 '{"type": "composer", "url": "https://gitlab.com/api/v4/group/612647/-/packages/composer/packages.json", "canonical": false}'
    fi

    $COMPOSER_EXECUTABLE config --working-dir=$dir --json extra.symfony.endpoint '["https://api.github.com/repos/xrowgmbh/recipes/contents/index.json?ref=main","https://api.github.com/repos/ibexa/recipes/contents/index.json?ref=flex/main","flex://defaults"]'

    ci_ibexa_add_bundle $dir ${bundle_name}
}

# Create a project using '$COMPOSER_EXECUTABLE create-project' followed by some modifications.
# Parameters:
# 1. Project directory
# 2. Package name (default: 'ibexa/oss-skeleton')
# 3. Version of project (default: 'dev-master')
# Use COMPOSER_ARGS to pass additional arguments to '$COMPOSER_EXECUTABLE create-project'.
ci_ibexa_build() {
  ci_ibexa_init_build_cache
  composer_auth
  echo "version Parameter #3 is $3"
  local dir="${1:-.}"
  echo "dir is $dir"

  local package="${2:-ibexa/oss-skeleton}"
  local version="${3:-dev-master}"

  echo "[info] PHP details:"
  php -v

  if [ -f /opt/app-root/etc/php.ini.template ]; then
    echo "[info] Initialise php.ini from php.ini.template."
    envsubst < /opt/app-root/etc/php.ini.template > ${PHP_SYSCONF_PATH}/php.ini
  fi;

  mkdir -p release || true
  rm -Rf $dir

  # Ibexa build composer.lock with PHP 8.3, but we build with PHP 8.1.
  # so run $COMPOSER_EXECUTABLE update to use dependencies that are compatible with PHP 8.1.
  # https://doc.ibexa.co/en/latest/getting_started/install_ibexa_dxp/#create-project
  echo "$(date +[%T];) Create project ${package}:$version."
  $COMPOSER_EXECUTABLE create-project ${COMPOSER_ARGS} ${package}:${version} --no-install ${dir}
  $COMPOSER_EXECUTABLE config --working-dir=$dir --no-plugins allow-plugins.php-http/discovery true

  echo -n "[info] composer version: "
  $COMPOSER_EXECUTABLE --version

  echo "[info] Remove $COMPOSER_EXECUTABLE post-update-cmd script."
  tmp=$(mktemp)
  jq '."scripts"."post-update-cmd" = []' $dir/composer.json > "$tmp" && mv -f "$tmp" $dir/composer.json

  echo "[info] Run '$COMPOSER_EXECUTABLE update' to get dependencies that are compatible with current PHP version."
  COMPOSER_MINIMAL_CHANGES=1 $COMPOSER_EXECUTABLE update --no-scripts --working-dir=$dir --with-all-dependencies ${COMPOSER_UPDATE_ARGS}

  echo "[info] Require symfony/messenger to ease possible integration with rabbitmq."
  $COMPOSER_EXECUTABLE require symfony/messenger --working-dir=$dir

  echo "[info] Project ${package}:$version created."
  shopt -s dotglob

  echo "[info] Copy files for S2I base setup."
  \cp -fR $TOOLS_DIR/scripts/files/ibexa/root/.[^.]* $dir/
  \cp -fR $TOOLS_DIR/scripts/files/ibexa/root/* $dir/

  echo "[info] Initialise git repository."
  # Why is this needed?
  GIT_DIR=$dir/.git
  git config --global init.defaultBranch main
  git init

  echo "[info] Reset $COMPOSER_EXECUTABLE post-update-cmd script."
  tmp=$(mktemp)
  jq '."scripts"."post-update-cmd" = ["@auto-scripts"]' $dir/composer.json > "$tmp" && mv -f "$tmp" $dir/composer.json

  echo "$(date +[%T];) Successfully created and modified project ${package}:$version."
}

# Add bundle using 'composer require'. Parameters:
# 1. Working directory.
# 2. Name of bundle to add.
# 3. Version of bundle.
# 4. Repository of bundle.
ci_ibexa_add_bundle() {
  composer_auth
  local dir="${1:-.}"

  local bundle
  if [ -v 2 ]; then
    bundle="$2";
  else
    echo "[error] Missing bundle parameter #2."
    exit 1
  fi;

  local version
  if [[ -v 3 ]]; then
    version="$3";
  elif [[ -v CI_COMMIT_SHA && -v CI_COMMIT_BRANCH ]]; then
    version="dev-${CI_COMMIT_BRANCH}#${CI_COMMIT_SHA}"
  elif [[ -v CI_COMMIT_SHA ]]; then
    version="dev-main#${CI_COMMIT_SHA}"
  else
    version="dev-main"
  fi;

  local repo
  if [ -v 4 ]; then
    repo="$4";
    $COMPOSER_EXECUTABLE config --working-dir=$dir repositories.$(echo $repo|base64) git $repo
  fi;

  echo "[info] Add bundle $bundle in version $version to directory $dir"
  local options="--working-dir=$dir --with-all-dependencies --minimal-changes"

  echo "[info] Running: $COMPOSER_EXECUTABLE require $options $bundle:$version"
  $COMPOSER_EXECUTABLE require $options $bundle:$version

  echo "[info] Install recipes"
  echo "$COMPOSER_EXECUTABLE --working-dir=$dir recipes"
  $COMPOSER_EXECUTABLE --working-dir=$dir recipes
}

# create DB and import DB dump
function ci_ibexa_mysql_insert_database() {
  FILE="${1:-data.sql.gz}"

  # Kill sleeping processes
  LIST=$(/usr/bin/mysql -e "show processlist;" | awk '/Sleep/ {print $1}')

  if [ -v LIST ]; then
    for i in $LIST ; do
      /usr/bin/mysql -e "KILL $i;" || true
    done
  fi

  /usr/bin/mysql -e "DROP DATABASE IF EXISTS ${DST_MYSQL_DATABASE}_tmp;"
  /usr/bin/mysql -e "CREATE DATABASE IF NOT EXISTS ${DST_MYSQL_DATABASE}_tmp CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
  /usr/bin/mysql -e "CREATE DATABASE IF NOT EXISTS ${DST_MYSQL_DATABASE} CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
  /usr/bin/mysql -e "ALTER DATABASE ${DST_MYSQL_DATABASE} CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"

  echo "$(date +[%T];) Importing data from '${FILE}' into temporary database '${DST_MYSQL_DATABASE}_tmp'."
  zcat ${FILE} | /usr/bin/mysql --init-command="SET @@session.unique_checks = 0;SET @@session.foreign_key_checks = 0;" ${DST_MYSQL_DATABASE}_tmp
  echo "$(date +[%T];) Import completed."

  echo "$(date +[%T];) Moving database tables from '${DST_MYSQL_DATABASE}_tmp' to '${DST_MYSQL_DATABASE}'."
  /usr/bin/mysql ${DST_MYSQL_DATABASE} -sNe 'show tables' | while read table; do /usr/bin/mysql -e "SET foreign_key_checks = 0;DROP TABLE IF EXISTS ${DST_MYSQL_DATABASE}.$table;SET foreign_key_checks = 1;"; done
  /usr/bin/mysql ${DST_MYSQL_DATABASE}_tmp -sNe 'show tables' | while read table; do /usr/bin/mysql -e "RENAME TABLE ${DST_MYSQL_DATABASE}_tmp.$table TO ${DST_MYSQL_DATABASE}.$table"; done
  echo "$(date +[%T];) Database move completed."

  /usr/bin/mysql -e "DROP DATABASE IF EXISTS ${DST_MYSQL_DATABASE}_tmp;"
  echo "$(date +[%T];) Dropped temporary database '${DST_MYSQL_DATABASE}_tmp'."
}

# Create a port-forwarding connection to the source MySQL database, and extract
# the database name into the variable SRC_MYSQL_DATABASE.
function ci_ibexa_create_src_database_connection() {
  kubectl config use-context $SRC_CONTEXT
  kubectl config set-context "$(kubectl config current-context)" --namespace=$SRC_NAMESPACE;
  ci_mysql_create_database_connection $SRC_NAMESPACE SRC_MYSQL_DATABASE
}

# Create a port-forwarding connection to the destination MySQL database, and extract
# the database name into the variable DST_MYSQL_DATABASE.
function ci_ibexa_create_dst_database_connection() {
  kubectl config use-context $DST_CONTEXT
  kubectl config set-context "$(kubectl config current-context)" --namespace=$DST_NAMESPACE;
  ci_mysql_create_database_connection $DST_NAMESPACE DST_MYSQL_DATABASE
}

# Look for an ibexa chart in the specified namespace.
# export variable RELEASE for further usage #20309
function ci_ibexa_get_chart_release() {
  if [ -z "$1" ]; then
    echo "[error] Missing namespace argument."
    exit 1
  fi

  export RELEASE=$(helm list -n ${1} -o json | jq -r '.[] | select(.chart | contains("ezplatform") or contains("ibexa"))' | jq -r '.name')
  if [ ! -v RELEASE ]; then
    echo "[error] No installed Helm chart found in namespace '${1}'."
    exit 1
  fi
  echo "[info] Found Helm chart in namespace '${1}' with release: '$RELEASE'."
}

# Check which suffix ("ezplatform" or "ibexa") to use depending on the chart version installed in a namespace #20309
# Export as variable CHART_NAME.
function ci_ibexa_get_chart_name() {
  if [ -z "$1" ]; then
    echo "[error] Missing namespace argument."
    exit 1
  fi

  if [ $(helm list -n ${1} -o json | jq -r '.[] | select(.chart | contains("ibexa"))' | jq -r '.name') ]; then
    export CHART_NAME="ibexa"
  elif [ $(helm list -n ${1} -o json | jq -r '.[] | select(.chart | contains("ezplatform"))' | jq -r '.name') ]; then
    export CHART_NAME="ezplatform"
  else
    echo "This is probably neither an ibexa nor an ezplatform chart - please check the field 'CHART' in the output of 'helm ls'."
    exit 1
  fi
}

# Sync database.
function ci_ibexa_sync_database() {
  echo "SRC_CONTEXT: $SRC_CONTEXT"
  echo "SRC_NAMESPACE: $SRC_NAMESPACE"
  echo "DST_CONTEXT: $DST_CONTEXT"
  echo "DST_NAMESPACE: $DST_NAMESPACE"

  # Install a local mysql client to connect to the database server
  ci_mysql_install_client

  # the temp directory used, within $DIR
  # omit the -p parameter to create a temporary directory in the default location
  WORK_DIR=`mktemp -d -p "$PWD"`

  # check if tmp dir was created
  if [[ ! "$WORK_DIR" || ! -d "$WORK_DIR" ]]; then
    echo "[error] Could not create temporary directory."
    exit 1
  fi

  # Kill any existing port-forwarding connections.
  ci_kubernetes_kill

  ci_ibexa_create_src_database_connection

  local dumpfile="${WORK_DIR}/data.sql.gz"

  echo "$(date +[%T];) Dump source database '${SRC_MYSQL_DATABASE}' into '${dumpfile}'."
  ci_mysql_dump_database ${dumpfile} ${SRC_MYSQL_DATABASE}

  ci_ibexa_create_dst_database_connection

  echo "$(date +[%T];) Insert data from '${dumpfile}' into destination database '${DST_MYSQL_DATABASE}'."
  ci_ibexa_mysql_insert_database ${dumpfile}

  ci_kubernetes_kill

  echo "$(date +[%T];) Next. Post data sync actions."

  ci_ibexa_get_chart_release $DST_NAMESPACE
  ci_ibexa_get_chart_name $DST_NAMESPACE

  ci_helm_run_hooks "data-sync" 0

  rm -rf "$WORK_DIR"
  echo "[info] Deleted temporary working directory '$WORK_DIR'."
  echo "$(date +[%T];) Completed data sync. All Done."
}

# Sync Ibexa file storage from one Kubernetes namespace to another.
function ci_ibexa_sync_files() {
  echo "SRC_CONTEXT: $SRC_CONTEXT"
  echo "SRC_NAMESPACE: $SRC_NAMESPACE"
  echo "DST_CONTEXT: $DST_CONTEXT"
  echo "DST_NAMESPACE: $DST_NAMESPACE"

  local PODNAME=sync-files

  # the temp directory used, within $DIR
  # omit the -p parameter to create a temporary directory in the default location
  local WORK_DIR=$(mktemp -d -p "$PWD")

  # check if tmp dir was created
  if [[ ! "$WORK_DIR" || ! -d "$WORK_DIR" ]]; then
    echo "[error] Could not create working temporary directory."
    exit 1
  fi

  mkdir -p $WORK_DIR/.ssh;
  mkdir -p $WORK_DIR/src-deploy;
  mkdir -p $WORK_DIR/dst-deploy;

  ssh-keygen -t rsa -b 4096 -P '' -f $WORK_DIR/.ssh/id_rsa
  chmod -R 600 $WORK_DIR/.ssh/id_rsa

  kubectl config use-context $SRC_CONTEXT
  ci_kubernetes_delete_pod $SRC_NAMESPACE $PODNAME

  ci_ibexa_get_chart_release $SRC_NAMESPACE
  ci_ibexa_get_chart_name $SRC_NAMESPACE

  echo "[info] Looking up volume claim in deployment '${RELEASE}-${CHART_NAME}-frontend' in namespace '$SRC_NAMESPACE'."
  export VOLUME_CLAIM="$(kubectl get deployment/${RELEASE}-${CHART_NAME}-frontend -n $SRC_NAMESPACE -o json | jq -r '.spec.template.spec.volumes[] | select(.name=="ibexa").persistentVolumeClaim.claimName')"

  echo "[info] Generating pod and configmap definitions '$PODNAME'."
  kubectl create configmap $PODNAME -n $SRC_NAMESPACE --from-file=$WORK_DIR/.ssh/id_rsa.pub --dry-run=client -o yaml > $WORK_DIR/src-deploy/configmap.yaml
  cp $TOOLS_DIR/scripts/files/sync/pod-template.yaml $WORK_DIR/src-deploy/pod.yaml

  # Substitute environment variables such as RELEASE, CHART_NAME and VOLUME_CLAIM in the pod definition
  ci_envsubst $WORK_DIR/src-deploy/pod.yaml

  echo "[info] Deploying pod and configmap in namespace '$SRC_NAMESPACE'."
  kubectl apply --wait=true -f $WORK_DIR/src-deploy -n $SRC_NAMESPACE

  POD=$(kubectl get pods -n $SRC_NAMESPACE | grep $PODNAME | awk '{print $1}')
  kubectl wait --for=condition=Ready pod/$POD --timeout=5m -n $SRC_NAMESPACE 2>&1
  echo "[info] Pod '$POD' in namespace '$SRC_NAMESPACE' is ready."

  SRC_PORT=$( expr $RANDOM % 1000 + 10000 )
  ci_kubernetes_port_forward $SRC_NAMESPACE $POD $SRC_PORT 2222 --pod-running-timeout=1m0s

  kubectl config use-context $DST_CONTEXT
  ci_kubernetes_delete_pod $DST_NAMESPACE $PODNAME
  helm list -n $DST_NAMESPACE -o json

  ci_ibexa_get_chart_release $DST_NAMESPACE
  ci_ibexa_get_chart_name $DST_NAMESPACE

  echo "[info] Looking up volume claim in deployment '${RELEASE}-${CHART_NAME}-frontend' in namespace '$DST_NAMESPACE'."
  export VOLUME_CLAIM="$(kubectl get deployment/${RELEASE}-${CHART_NAME}-frontend -n $DST_NAMESPACE -o json | jq -r '.spec.template.spec.volumes[] | select(.name=="ibexa").persistentVolumeClaim.claimName')"

  echo "[info] Generating pod and configmap definitions '$PODNAME'."
  kubectl create configmap $PODNAME -n $DST_NAMESPACE --from-file=$WORK_DIR/.ssh/id_rsa.pub --dry-run=client -o yaml > $WORK_DIR/dst-deploy/configmap.yaml
  cp $TOOLS_DIR/scripts/files/sync/pod-template.yaml $WORK_DIR/dst-deploy/pod.yaml
  ci_envsubst $WORK_DIR/dst-deploy/pod.yaml

  echo "[info] Deploying pod and configmap in namespace '$DST_NAMESPACE'."
  kubectl apply --wait=true -f $WORK_DIR/dst-deploy -n $DST_NAMESPACE

  POD=$(kubectl get pods -n $DST_NAMESPACE | grep $PODNAME | awk '{print $1}')
  kubectl wait --for=condition=Ready pod/$POD --timeout=5m -n $DST_NAMESPACE 2>&1
  echo "[info] Pod '$POD' in namespace '$DST_NAMESPACE' is ready."

  DST_PORT=$( expr $RANDOM % 1000 + 20000 )
  ci_kubernetes_port_forward $DST_NAMESPACE $POD $DST_PORT 2222 --pod-running-timeout=1m0s

  echo "[info] File sync from localhost:$SRC_PORT to localhost:$DST_PORT"

  #echo "Test connection and show date"
  #ssh -vvv -o GlobalKnownHostsFile=/dev/null -o UserKnownHostsFile=/dev/null -i $WORK_DIR/.ssh/id_rsa -p $SRC_PORT root@127.0.0.1 'date'
  #ssh -vvv -o GlobalKnownHostsFile=/dev/null -o UserKnownHostsFile=/dev/null -i $WORK_DIR/.ssh/id_rsa -p $DST_PORT root@127.0.0.1 'date'

  echo "[info] Copy SSH settings."

  scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -r -P $SRC_PORT -i $WORK_DIR/.ssh/id_rsa $WORK_DIR/.ssh root@127.0.0.1:/root
  scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -r -P $DST_PORT -i $WORK_DIR/.ssh/id_rsa $WORK_DIR/.ssh root@127.0.0.1:/root

  echo "$(date +[%T];) Start copying files."

  ssh -A -R 127.0.0.1:50000:127.0.0.1:$DST_PORT root@127.0.0.1 -p $SRC_PORT -i $WORK_DIR/.ssh/id_rsa -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
      'rsync --ignore-missing-args --no-compress -W --info=progress2 --info=name0 -e "ssh -i /root/.ssh/id_rsa -p 50000 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" -av -P /mnt/* root@127.0.0.1:/mnt'

  retval=$?
  if [[ "$retval" != "0" && "$retval" != "24" ]]; then
      echo "[error] File sync job failed. Please check logs."
      exit 1
  fi

  echo "$(date +[%T];) File sync completed."

  # deletes the temp directory
  rm -rf "$WORK_DIR"
  echo "[info] Deleted temporary working directory '$WORK_DIR'."

  kubectl config use-context $SRC_CONTEXT
  ci_kubernetes_delete_pod $SRC_NAMESPACE $PODNAME

  kubectl config use-context $DST_CONTEXT
  ci_kubernetes_delete_pod $DST_NAMESPACE $PODNAME
}

# Should this function be removed or fixed?
function ci_ibexa_setup() {
  echo "[warning] ci_ibexa_setup is no longer supported"

  local release="${1:-$CI_PROJECT_NAME}"
  local namespace="${2:-$DST_NAMESPACE}"

  # This will fail, the second argument should be a configuration file e.g. 'deploy/stage.yml'
  ci_helm_deploy_chart $release $namespace "true"

  echo "[info] @TODO Bug with MySQL check, it hangs when the kubectl server version is too old. Skipping."

  # if [[ "$( kubectl get pod -l app.kubernetes.io/name=mysql -o jsonpath='{.items[0].spec.containers[0].name}' )" != "" ]]; then
  #   local mysql=$(kubectl get pods -l app.kubernetes.io/name=mysql -o jsonpath='{.items[0].metadata.name}')
  #   kubectl wait --for=condition=Ready pod/$mysql --timeout=5m 2>&1
  #   while [[ true ]]; do
  #       sleep 5
  #       set +e
  #       kubectl logs -l app.kubernetes.io/name=mysql | grep -i "'/var/run/mysqld/mysqld.sock'  port: 3306"
  #       test=$?
  #       set -e
  #       if [[ "$test" == 0 ]]; then
  #         echo "MySQL done. MySQL is up with initial data. Tested for string /var/run/mysqld/mysqld.sock."
  #         break
  #       fi
  #   done
  # fi
  echo "[info] Post data sync actions."
  echo "[info] Check if hook post-pipeline-install exists in composer.json"

  echo "@TODO rework pipeline integration"
  # the following statement checks which suffix ("ezplatform"/"ibexa") to use on the target system ("DST") depending on the chart version #20309
  if [ $(helm list -o json | jq -r '.[] | select(.chart | contains("ibexa"))' | jq -r '.name') ]; then
    export CHART_NAME="ibexa"
  elif [ $(helm list -o json | jq -r '.[] | select(.chart | contains("ezplatform"))' | jq -r '.name') ]; then
    export CHART_NAME="ezplatform"
  else
    echo "[warning] This chart is not known to have pipeline integration."
  fi

  POD=$(kubectl get pods -n $namespace --output=name --field-selector=status.phase!=Terminating | grep $CHART_NAME-frontend | awk '{print $1}' | tail -n 1 | sed -e "s/^pod\///" )
  echo "Waiting for pod(s) '$POD' to start up."
  kubectl wait --for=condition=Ready pod/$POD --timeout=5m -n $namespace 2>&1
  echo "'$POD' ready."

  kubectl cp $namespace/$POD:composer.json composer.json
  if [[ $( cat composer.json | jq '.scripts."post-pipeline-install"' ) != "null" ]]; then
    kubectl exec -n $namespace pod/$POD -- bash -c "$COMPOSER_EXECUTABLE run-script --timeout=0 post-pipeline-install;"
    if [[ "0" != $?  ]]; then
      echo "$(date +[%T];) Error running $COMPOSER_EXECUTABLE run-script --timeout=0 post-pipeline-install"
      exit 1;
    fi
  fi;

  echo "$(date +[%T];) Done. Post data sync actions."
  echo "$(date +[%T];) All Done."
}
