#!/bin/bash

function test_init() {
  if [[ -v CI_PROJECT_DIR ]]; then
    cp -R -f --preserve=all $TOOLS_DIR/scripts/config-root/* ${CI_PROJECT_DIR}
    cp -R -f --preserve=all $TOOLS_DIR/scripts/config-root/.[^.]* ${CI_PROJECT_DIR}
  fi
}

function test_install_gpg() {
  mkdir -p $HOME/.gnupg | true
  echo "batch" > $HOME/.gnupg/gpg.conf
  find ~/.gnupg -type f -exec chmod 600 {} \; # Set 600 for files
  find ~/.gnupg -type d -exec chmod 700 {} \; # Set 700 for directories
  # @TODO will hang in docker
  # gpg --generate-key
}

function test_install_phive() {
  test_install_gpg
  wget -O phive.phar "https://phar.io/releases/phive.phar"
  wget -O phive.phar.asc "https://phar.io/releases/phive.phar.asc"
  gpg --keyserver hkps://keys.openpgp.org --recv-keys 0x6AF725270AB81E04D79442549D8A98B29B2D5D79
  gpg --verify phive.phar.asc phive.phar
  rm -Rf phive.phar.asc
  rm -Rf $HOME/.bin/phive
  chmod +x phive.phar
  mkdir -p $HOME/.bin || true
  mv phive.phar $HOME/.bin/phive
  export PATH="$PATH:$HOME/.bin"
  echo $PATH
  echo "Installed phive"
}

function box_install_box() {
  test_install_phive
  phive install --target /usr/bin humbug/box
}

function test_rector() {
  composer_auth
  composer require rector/rector --dev
  vendor/bin/rector init
  php vendor/bin/rector
}

# Download and run the PHP Mess Detector
function test_phpmd() {
  local dir="${1:-src}"
  # @TODO After release 2.13 https://github.com/phpmd/phpmd/blob/master/src/site/rst/documentation/ci-integration.rst#gitlab-code-quality-reporting
  wget -c https://phpmd.org/static/latest/phpmd.phar

  # We allow developers to suppress warnings so don't use the --strict option.
  php phpmd.phar $dir ansi $TOOLS_DIR/scripts/config-root/phpmd.xml
}

# Run the PHP Copy/Paste Detector
function test_phpcpd() {
  local dir="${1:-.}"
  wget -c https://phar.phpunit.de/phpcpd.phar
  php phpcpd.phar --fuzzy --suffix .php --suffix .twig --suffix .scss --suffix .css $dir/src/
  php phpcpd.phar --fuzzy --suffix .twig $dir/src/
}

function test_php_codequality() {
  local dir="${1:-.}"
  test_install_phive
  TRUSTEES="51C67305FFC2E5C0"
  phive install --trust-gpg-keys $TRUSTEES --copy phpstan -t /tmp/phpstan
  rm -Rf ${dir}/phpstan.neon
  cp $TOOLS_DIR/scripts/config-root/phpstan.neon ${dir}/phpstan.neon
  set +e
  /tmp/phpstan/phpstan analyse -c ${dir}/phpstan.neon --error-format=gitlab ${dir} > $CI_PROJECT_DIR/gl-code-quality-report.json
  RETURN=$?
  set -e
  if [[ -f $CI_PROJECT_DIR/gl-code-quality-report.json ]]; then
    echo "[info] $CI_PROJECT_DIR/gl-code-quality-report.json is generated"
  fi
  /tmp/phpstan/phpstan analyse -c ${dir}/phpstan.neon ${dir}
}
function test_php_codestyle() {
  test_install_phive
  echo $PATH

  phive install --trust-gpg-keys E82B2FB314E9906E --target $HOME/.bin php-cs-fixer
  cat $TOOLS_DIR/scripts/config-root/.php-cs-fixer.php > .php-cs-fixer.php
  echo 'Running PHP CS Fixer with config:'
  echo
  cat .php-cs-fixer.php
  echo
  php-cs-fixer fix --dry-run --diff -v;
  RETURN=$?;
  if [[ $RETURN == 0 ]]; then
    echo 'Code style is good.';
    return 0;
  else
    echo "Run 'php-cs-fixer fix' to fix your code";
    return 1;
  fi
}

# Test Symfony translations
function test_symfony_translation() {
  local dir="${1:-.}"
  cd $dir
  $(which php) bin/console debug:translation en --only-missing || error_code=$?
  if [[ "${error_code}" -eq 66  ]]; then
    :
  elif [[ "${error_code}" -eq 0 ]]; then
    :
  else
    echo "Missing translations."; exit 1;
  fi

  #  In some projects, German is not the primary language and does not even exist. 
  #  Therefore, forcing a check of the German translation results will cause continuous 
  #  errors and prevent the job from passing successfully. 
  #  The check for German is temporarily blocked here.
  #
  #  $(which php) bin/console debug:translation de --only-missing || error_code=$?
  #  if [[ "${error_code}" -eq 66  ]]; then
  #    :
  #  elif [[ "${error_code}" -eq 0 ]]; then
  #    :
  #  else
  #    echo "Missing translations."; exit 1;
  #  fi

  #  This is xrow company policy:
  #    1) Do not let devs use YAML for translations
  #    2) Do not let devs name XLIFF using .xliff
  #  So if a bundle's translation folders contain *.yml or *.yaml or *.xliff then the job should return an error exit code (1).
  lines=$( find ./{translations,src}/ -type f \( -name "*.en.yml" -o -name "*.en.yaml" \) | wc -l)
  if [ $lines -gt 0 ]; then
    echo "Only use XLIFF / .xlf for translation"; exit 1;
  fi
  lines=$( find ./{translations,src}/ -type f -name "*.en.xliff" | wc -l)
  if [ $lines -gt 0 ]; then
    echo "Rename .xliff to .xlf for translation"; exit 1;
  fi
}
