#!/bin/bash

# Define some constants

export CI_PATTERN_TRUNK='^(master|main)$'
export CI_PATTERN_STABLE_BRANCH='^(0|[1-9]\d*)\.(0|[1-9]\d*)$'

# https://semver.org/#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string
# https://github.com/har7an/bash-semver-regex/blob/main/test.sh
# Regex for a semver digit
export CI_PATTERN_SEMVER_DIGIT='0|[1-9][0-9]*'
# Regex for a semver pre-release word
export CI_PATTERN_SEMVER_PRERELEASE='[0-9]*[a-zA-Z-][0-9a-zA-Z-]*'
# Regex for a semver build-metadata word
export CI_PATTERN_SEMVER_METADATA='[0-9a-zA-Z-]+'
export CI_PATTERN_SEMVER_VERSION="($CI_PATTERN_SEMVER_DIGIT)\\.($CI_PATTERN_SEMVER_DIGIT)\\.($CI_PATTERN_SEMVER_DIGIT)"
export CI_PATTERN_SEMVER_BUILD='(-(('$CI_PATTERN_SEMVER_DIGIT'|'$CI_PATTERN_SEMVER_PRERELEASE')(\.('$CI_PATTERN_SEMVER_DIGIT'|'$CI_PATTERN_SEMVER_PRERELEASE'))*))?(\+('$CI_PATTERN_SEMVER_METADATA'(\.'$CI_PATTERN_SEMVER_METADATA')*))?'
export CI_PATTERN_SEMVER=${CI_PATTERN_SEMVER_VERSION}${CI_PATTERN_SEMVER_BUILD}

source $TOOLS_DIR/scripts/lib/certificate.sh
source $TOOLS_DIR/scripts/lib/gitlab.sh
source $TOOLS_DIR/scripts/lib/util.sh
source $TOOLS_DIR/scripts/lib/env.sh
source $TOOLS_DIR/scripts/lib/azure.sh
source $TOOLS_DIR/scripts/lib/cosign.sh
source $TOOLS_DIR/scripts/lib/trycatch.sh
source $TOOLS_DIR/scripts/lib/dev.sh
source $TOOLS_DIR/scripts/lib/gitpod.sh
source $TOOLS_DIR/scripts/lib/ansible.sh
source $TOOLS_DIR/scripts/lib/database.sh
source $TOOLS_DIR/scripts/lib/lint.sh
source $TOOLS_DIR/scripts/lib/test.sh
source $TOOLS_DIR/scripts/lib/test_bundle.sh
source $TOOLS_DIR/scripts/lib/s2i.sh
source $TOOLS_DIR/scripts/lib/container.sh
source $TOOLS_DIR/scripts/lib/helm.sh
source $TOOLS_DIR/scripts/lib/kubernetes.sh
source $TOOLS_DIR/scripts/lib/composer.sh
source $TOOLS_DIR/scripts/lib/ibexa.sh
source $TOOLS_DIR/scripts/lib/documentation.sh
source $TOOLS_DIR/scripts/lib/operator.sh
source $TOOLS_DIR/scripts/lib/ssh.sh
source $TOOLS_DIR/scripts/lib/review.sh
source $TOOLS_DIR/scripts/lib/cypress.sh
source $TOOLS_DIR/scripts/lib/git.sh
source $TOOLS_DIR/scripts/lib/vault-secrets.sh

function ci_init() {
  if [[ -v CI_JOB_NAME ]]; then
    echo "[info] Documentation of job '${CI_JOB_NAME}': https://xrow-public.gitlab.io/ci-tools/#${CI_JOB_NAME}"
  fi
  [ "${BASH_VERSINFO:-0}" -lt 4 ] && echo "Bash versions lesser 5 are not supported." && exit 1


  # Some extra global variables
  export CI_CONTAINER_RELEASE_TAG="${CI_CONTAINER_RELEASE_TAG:-$(ci_container_get_release_tag)}"
  export CI_HELM_RELEASE_TAG="${CI_HELM_RELEASE_TAG:-$(ci_helm_get_release_tag)}"

  export BUILD_CACHE_DIR="${BUILD_CACHE_DIR:-/cache}"
  if [[ -d $BUILD_CACHE_DIR ]]; then
    echo "[info] Cache dir $BUILD_CACHE_DIR is present."
    echo "Permissions of $BUILD_CACHE_DIR"
    ls -lda $BUILD_CACHE_DIR
    export XDG_CACHE_HOME=$BUILD_CACHE_DIR
    # mkdir -p /cache/dnf || true
    # mount --rbind /cache/dnf /var/cache/dnf
  else
    echo "[info] Cache dir $BUILD_CACHE_DIR is not present."
    unset BUILD_CACHE_DIR
  fi;
  if [[ ${CI_DEBUG_TRACE} == "true" ]]; then
    echo "[warn] Debug mode is enabled. Debug mode exposes security tokens. Set 'CI_DEBUG_TRACE=false' to disable"
    ci_debug
  else
    echo "[info] Debug mode is disabled. Set 'CI_DEBUG_TRACE=true' to enable."
  fi
  return 0
}

function ci_exit_or_return() {
  if [[ -v CI_RUNNER_VERSION ]]; then
    exit 1
  fi;
  return 1
}
