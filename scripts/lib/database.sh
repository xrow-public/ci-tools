#!/bin/bash

# Dump the database in the specified namespace.
function ci_database_snapshot() {
  type="${1:-mysql}"
  local namespace="${2:-$SRC_NAMESPACE}"
  local work_dir="${3:-}"

  if [[ $type != "mysql" ]]; then
    echo "'$type' not implemented"
    exit 1
  fi

  if [[ -z "$namespace" ]]; then
    echo "[error] namespace is not set."
    exit 1
  fi;

  if [[ -z "$work_dir" ]]; then
    work_dir=`mktemp -d -p "$PWD"`
    echo "[info] Created temporary directory '$work_dir'."
  else
    mkdir -p ${work_dir}
    echo "[info] Created work directory '$work_dir'."
  fi

  # Install a local client to connect to the database server
  ci_mysql_install_client

  # Kill any existing port-forwarding connections.
  ci_kubernetes_kill

  ci_mysql_create_database_connection $namespace
  ci_mysql_dump_database ${work_dir}/data.sql.gz

  ci_kubernetes_kill

  ls -l ${work_dir}
}

# Build and push a database container with the snapshot from the specified directory
function ci_database_push() {
  type="${1:-mysql}"
  local work_dir="${2:-}"

  if [[ $type != "mysql" ]]; then
    echo "'$type' not implemented."
    exit 1
  fi

  ls -l $work_dir

  if [[ ! -f $work_dir/data.sql.gz ]]; then
    echo "Database dump 'data.sql.gz' not found in directory $work_dir"
    exit 1
  fi

  echo "CI_CONTAINERFILE: $CI_CONTAINERFILE"
  echo "[info] bash: set -e"
  set -e

  podman login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  if [[ -v CI_CONTAINERFILE && -f $CI_CONTAINERFILE ]]; then
    cp $CI_CONTAINERFILE $work_dir/Containerfile
  else
    echo "FROM docker.io/mysql:8.0" >> $work_dir/Containerfile
    echo "USER root" >> $work_dir/Containerfile
    echo "COPY data.sql.gz /docker-entrypoint-initdb.d/data.sql.gz" >> $work_dir/Containerfile
  fi

  podman build --format=docker -f $work_dir/Containerfile --no-cache -t $CI_REGISTRY_IMAGE/database:latest $work_dir/
  podman push $CI_REGISTRY_IMAGE/database:latest
  echo "$(date +[%T];) Saved database image as $CI_REGISTRY_IMAGE/database:latest"
  rm -Rf $work_dir/data.sql.gz
  echo "$(date +[%T];) Deleted file $work_dir/data.sql.gz."
}

# Install mysql client locally
function ci_mysql_install_client() {
  echo "$(date +[%T];) Installing mysql client locally..."
  rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2023
  dnf install -y https://dev.mysql.com/get/mysql80-community-release-el9-5.noarch.rpm
  dnf install -y mysql-community-client
}

# Write a MySQL configuration file containing connection details:
# - username
# - password
# - host
# - port
function ci_mysql_setup_client() {
  rm -Rf ~/.my.cnf
  cat << EOF > ~/.my.cnf
[client]
user=${1}
password=${2}
host=${3:-127.0.0.1}
port=${4:-3306}
EOF
  chmod 600 ~/.my.cnf
  echo "[info] MySQL client configuration:"
  sed 's/password=.*$/password=******/g;s/^/    /' ~/.my.cnf
}

# Create a port-forwarding connection to the source MySQL database in the namespace
# defined by the first argument, and export the database name into a variable
# defined by the second argument.
function ci_mysql_create_database_connection() {
  local namespace="${1:-$SRC_NAMESPACE}"
  local dbnamevar="${2:-SRC_MYSQL_DATABASE}"

  # Find the database pod in the namespace (its name should contain 'mysql').
  local pod=$(kubectl get pods -n ${namespace} | grep mysql | awk '{print $1}')

  # Ensure the database pod is ready.
  echo "[info] Wait for pod '$pod' in namespace '$namespace' to become ready."
  kubectl wait --for=condition=Ready pod/${pod} --timeout=2m -n ${namespace} 2>&1

  # the following logic uses JSON output to get the individual mysql container name in each project
  local container=$(kubectl get pod ${pod} -n ${namespace} -o jsonpath='{.spec.containers[*].name}' | awk '{print $1}')

  local mysql_password=$(kubectl exec -c ${container} -n ${namespace} pod/${pod} -- bash -c "echo \$MYSQL_ROOT_PASSWORD;")
  export ${dbnamevar}=$(kubectl exec -c ${container} -n ${namespace} pod/${pod} -- bash -c "echo \$MYSQL_DATABASE;")

  # Create a connection to the database pod
  local port=$( expr $RANDOM % 1000 + 10000 )
  ci_kubernetes_port_forward ${namespace} ${pod} ${port} 3306

  # Configure the client to connect to the database server
  ci_mysql_setup_client root ${mysql_password} 127.0.0.1 ${port}

  echo "[info] Test MySQL connection."
  mysql -e "SELECT 1;"
}

# Dump the specified database, or all databases if none specified.
# Arguments:
# - name of file to dump into (optional)
# - name of database to dump (optional)
function ci_mysql_dump_database() {
  if [[ ! -x "$(command -v mysqldump)" ]]; then
    echo '[error] mysqldump is not installed.' >&2
    exit 1
  fi

  FILE="${1:-data.sql.gz}"
  if [[ -f "$FILE" ]]; then
    echo "[info] Database dump file '$FILE' already exists. Nothing to do."
    return 0
  fi

  set -o pipefail

  if [ -v 2 ]; then
    echo "$(date +[%T];) Dumping database: $2"
    mysqldump --opt --compression-algorithms=zstd --column-statistics=0 --single-transaction $2 > tmp.sql
  else
    # Get a list of all databases
    databases=$(mysql -e "SHOW DATABASES;" | tr -d "| " | grep -v Database)

    # Dump each database in turn
    rm -f tmp.sql
    for db in $databases; do
      if [[ "$db" != "information_schema" ]] && [[ "$db" != "performance_schema" ]] && [[ "$db" != "mysql" ]] && [[ "$db" != "sys" ]]; then
        echo "$(date +[%T];) Dumping database: $db"
        mysqldump --opt --compression-algorithms=zstd --column-statistics=0 --single-transaction $db >> tmp.sql
      fi
    done
  fi

  gzip -cvf tmp.sql > ${FILE}
  rm -f tmp.sql
  set +o pipefail
  echo "$(date +[%T];) Completed database dump."
}
