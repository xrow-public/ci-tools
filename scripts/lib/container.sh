#!/bin/bash

export CONTAINER_RUNTIME="${CONTAINER_RUNTIME:-podman}"
export CONTAINER_RUNTIME_PUSH="${CONTAINER_RUNTIME_PUSH:-podman push --format oci}"
export CONTAINER_BUILD_OPTIONS="${CONTAINER_BUILD_OPTIONS:-}"
# @TODO find a way to cache to work with --squash:  --layers --cache-to $CI_REGISTRY/$CI_PROJECT_PATH/cache --cache-from $CI_REGISTRY/$CI_PROJECT_PATH/cache
# Error: cannot specify --squash with --layers and --squash-all with --squash
export CONTAINER_BUILD="${CONTAINER_BUILD:-podman build --network host --format oci --squash}"

function ci_registry_token_login() {
  $CONTAINER_RUNTIME logout "$CI_REGISTRY" > /dev/null 2>&1 || true
  if [[ -v RH_REGISTRY_ACCOUNT && -v RH_REGISTRY_PASSWORD ]]; then
    echo "${RH_REGISTRY_PASSWORD}" | $CONTAINER_RUNTIME login -u ${RH_REGISTRY_ACCOUNT} --password-stdin registry.redhat.io > /dev/null 2>&1 || echo "[warn] Login to registry registry.redhat.io failed"
  elif [[ -v RH_USERNAME && -v RH_PASSWORD ]]; then
    echo "${RH_PASSWORD}" | $CONTAINER_RUNTIME login -u ${RH_USERNAME} --password-stdin registry.redhat.io > /dev/null 2>&1 || echo "[warn] Login to registry registry.redhat.io failed"
  else
    echo "[info] RH_USERNAME or RH_REGISTRY_ACCOUNT is not set or protected. registry.redhat.io will be not available."
  fi
  if [[ -v CI_REGISTRY_PASSWORD && -v CI_REGISTRY_USER ]]; then
    echo "[info] Using login '$CI_REGISTRY_USER' for $CI_REGISTRY"
    # Permission workaround for Bug https://gitlab.com/gitlab-org/gitlab/-/issues/444268 might be relevant
    $CONTAINER_RUNTIME login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY > /dev/null 2>&1 || echo "[warn] Login to registry $CI_REGISTRY failed"
  elif [[ -v CI_REGISTRY_USER && -v CI_JOB_TOKEN ]]; then
    echo "[info] Login for GitLab registry $CI_REGISTRY"
    $CONTAINER_RUNTIME login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY > /dev/null 2>&1 || echo "[warn] Login to registry $CI_REGISTRY failed"
  fi
}

ci_container_export_source(){
  local dir="${1:-./}"
  local image="${2:-$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA}"
  $CONTAINER_RUNTIME pull $image
  mnt=$($CONTAINER_RUNTIME image mount $image)
  mkdir -p $dir || true
  cp -r $mnt/opt/app-root/src/* $dir
}

function ci_container_build() {
  local dir="${1:-./}"
  local containerfile="${2:-}"

  echo "CI_REGISTRY_IMAGE: $CI_REGISTRY_IMAGE"
  echo "CI_COMMIT_SHA: $CI_COMMIT_SHA"
  echo "args: $BUILD_ARGS"
  echo "Context directory: $dir"

  local args=()
  if [ -n "$BUILD_ARGS" ]; then
    local tmp=$(mktemp -p /tmp)
    args+=( "--build-arg-file=$tmp" )
    for arg in $BUILD_ARGS; do
        echo -n "${arg}=" >> $tmp
        echo -n "${!arg}" >> $tmp
        echo "" >> $tmp
    done
  fi;

  if [[ -n "$BUILD_CACHE_DIR" ]]; then 
    args+=( "--build-arg" )
    args+=( "XDG_CACHE_HOME=${BUILD_CACHE_DIR}" )
    args+=( "-v" )
    args+=( "${BUILD_CACHE_DIR}:${BUILD_CACHE_DIR}:z" )
  fi

  if [[ -n "$containerfile" ]]; then 
    args+=( "--file=$containerfile" )
  fi

  if [[ "$(ci_container_exists)" == "1" ]]; then
    ci_container_export_appversion "./release/${name}"
    return 0
  fi;

  ci_registry_token_login
  echo "Full build command: $CONTAINER_BUILD $CONTAINER_BUILD_OPTIONS $CONTAINER_BUILD_ARGS ${args[@]} -t $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $dir"
  rm -Rf ~/.local/share/containers/
  $CONTAINER_BUILD $CONTAINER_BUILD_OPTIONS $CONTAINER_BUILD_ARGS "${args[@]}" -t $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $dir

  # This is the only and first time we push CI_COMMIT_SHA
  echo "[current] Pushing build version $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA"
  $CONTAINER_RUNTIME_PUSH $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  echo "[current] Pushed build version $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA"

  ci_container_export_appversion "./release/${name}"
  echo "[info] Build done."
}

function ci_container_export_appversion() {
  local dir="${1:-./release}"
  local version

  $CONTAINER_RUNTIME pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  mnt=$($CONTAINER_RUNTIME image mount $image 2> /dev/null || echo "")
  mkdir -p $dir || true
  if [[ -f $mnt/opt/appversion ]]; then 
    version="$(cat $mnt/opt/appversion)" 
  fi

  if [[ -n "$version" ]]; then
    echo "[info] Found appversion: $version"
    mkdir -p $dir || true
    echo "$version" >> $dir/appversion
  else
    echo "[info] Found no appversion"
  fi
}

function ci_container_exists() {
  ci_registry_token_login
  set +e
  $CONTAINER_RUNTIME manifest inspect $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA &> /dev/null ; EXISTS=$?;
  set -e
  if [[ "$EXISTS" == "0" ]]; then
    echo "[info] Image does not exist."
    return 0
  elif [[ $CI_JOB_MANUAL == "true" ]]; then
    echo "[info] User pushed a button something went wrong."
    return 0
  elif [[ $CI_PIPELINE_SOURCE == "web" ]]; then
    echo "[info] CI_PIPELINE_SOURCE was web."
    return 0
  fi;
  echo "[info] Image $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA already exists."
  return 1
}

# Output a value for a release tag based the current commit SHA, tag or branch.
ci_container_get_release_tag() {
  local tag
  local branch

  # CI_COMMIT_BRANCH is not available in merge request pipelines or tag pipelines
  branch=$( sed -E 's/[\/]/./g;' <<< "${CI_COMMIT_BRANCH}" )

  if [[ -v CI_COMMIT_TAG  ]]; then
    tag=${CI_COMMIT_TAG}
  elif [[ -v CI_COMMIT_SHA ]]; then
    if [ -n "$branch" ]; then
      tag="${branch}.${CI_COMMIT_SHA}"
    else
      tag="${CI_COMMIT_SHA}"
    fi
  else
    tag="${CI_DEFAULT_BRANCH:-main}"
  fi

  echo "${tag}"
  return 0
}

# Push the current container image (defined by CI_REGISTRY_IMAGE and CI_COMMIT_SHA).
# Arguments:
# 1. Should the image be tagged as latest?
# 2. Is the image a release (defined by CI_RELEASE_REGISTRY_IMAGE)?
function ci_container_push_build() {
  ci_registry_token_login

  echo "$(date +[%T];) Container pushing."

  echo "[info][tag] Pushing $CI_REGISTRY_IMAGE:$CI_CONTAINER_RELEASE_TAG"
  $CONTAINER_RUNTIME tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_CONTAINER_RELEASE_TAG
  $CONTAINER_RUNTIME_PUSH $CI_REGISTRY_IMAGE:$CI_CONTAINER_RELEASE_TAG
  echo "[info][tag] Pushed $CI_REGISTRY_IMAGE:$CI_CONTAINER_RELEASE_TAG"

  if [[ -v $CI_COMMIT_TAG && "$CI_DEFAULT_BRANCH" == "$CI_COMMIT_BRANCH" ]]; then
    echo "[info][latest] Pushing $CI_REGISTRY_IMAGE:latest"
    $CONTAINER_RUNTIME tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:latest
    $CONTAINER_RUNTIME_PUSH $CI_REGISTRY_IMAGE:latest
    echo "[info][latest] Pushed $CI_REGISTRY_IMAGE:latest"
  fi;

  if [[ -v CI_COMMIT_REF_SLUG && ( "$CI_COMMIT_REF_SLUG" =~ ${CI_PATTERN_STABLE_BRANCH} || "$CI_COMMIT_REF_SLUG" =~ ${CI_PATTERN_TRUNK} ) ]] ; then 
    echo "[info][branch] Pushing $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
    $CONTAINER_RUNTIME tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    $CONTAINER_RUNTIME_PUSH $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    echo "[info][branch] Pushed $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
  fi;

  echo "$(date +[%T];) Push done."
}