#!/bin/bash

function ci_util_init() {
  if [[ ! -x "$(command -v envsubst)" ]]; then
    dnf install gettext -y
  fi
}

function ci_envsubst {
  local file="${1}"
  local dest="${2:-$file}"
  ci_util_init
  if [[ -f "${file}" ]]; then
    local tmpfile
    tmpfile=$(mktemp /tmp/ci_envsubst.XXXXXX)
    cat "${file}" > "${tmpfile}"
    # shellcheck disable=SC2002
    cat "${tmpfile}" | envsubst "$(awk 'BEGIN { for (var in ENVIRON) printf "$%s ", var; print "" }' </dev/null)" > "${dest}"
    echo "[info] Modified: ${dest} with size of $(stat -c %s "${dest}") bytes"
    rm -Rf "${tmpfile}"
  fi
}