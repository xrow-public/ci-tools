#!/bin/bash

function test_symfony_bundle_translation() {
  name="$(find -name *Bundle.php | sed -En 's/.\/(.*).php/\1/p')"
  local dir="${1:-.}"
  cd $dir
  $(which php) bin/console debug:translation en $name --only-missing || error_code=$?
  if [[ "${error_code}" -eq 66  ]]; then
    :
  elif [[ "${error_code}" -eq 0 ]]; then
    :
  else
    echo "Missing translations."; exit 1;
  fi
  $(which php) bin/console debug:translation de $name --only-missing || error_code=$?
  if [[ "${error_code}" -eq 66  ]]; then
    :
  elif [[ "${error_code}" -eq 0 ]]; then
    :
  else
    echo "Missing translations."; exit 1;
  fi
  lines=$( find ./{translations,src}/ -type f -name "*.en.yml" | wc -l)
  if [ $lines -eq 0 ]; then
    echo "Only use XLIFF / .xlf for translation"; exit 1;
  fi
  lines=$( find ./{translations,src}/ -type f -name "*.en.xliff" | wc -l)
  if [ $lines -eq 0 ]; then
    echo "Rename .xliff to .xlf for translation"; exit 1;
  fi
}

function test_bundle_phpmd() {
  # @TODO After releae 2.13 https://github.com/phpmd/phpmd/blob/master/src/site/rst/documentation/ci-integration.rst#gitlab-code-quality-reporting
  wget -c https://phpmd.org/static/latest/phpmd.phar
  # We allow developers to suppress warnings so don't use the --strict option.
  php phpmd.phar . ansi $TOOLS_DIR/scripts/config-root/phpmd.xml
}