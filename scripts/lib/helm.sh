#!/bin/bash

export HELM_EXPERIMENTAL_OCI="1"
export HELM_TIMEOUT="${HELM_TIMEOUT:-10m}"
export CI_SHARED_REPOID="40219561"
export CI_PUBLIC_REPOID="29805218"
export CI_SHARED_REPO_URL="https://$CI_SERVER_HOST/xrow-shared/repository/-/packages"

if [[ -d $BUILD_CACHE_DIR && ! -v HELM_CACHE_HOME ]]; then
  export HELM_CACHE_HOME=$BUILD_CACHE_DIR
fi

function ci_helm_install() {
  if [[ ! -x "$(command -v helm)" ]]; then
    export HELM_INSTALL_DIR="/usr/local/bin"
    curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
  fi
}

function ci_helm_push_latest() {
  local dir="${1:-./}"
  repo="$CI_REGISTRY/$CI_REGISTRY_PROJECT_PATH/charts"
  local chart_name="$(yq -r '.name' ${dir}Chart.yaml)"
  helm package ${dir} --version 0.0.0 --destination .
  echo "[push] oci://$repo/$chart_name:0.0.0"
  helm push $chart_name-0.0.0.tgz oci://$repo
  rm -Rf $chart_name-0.0.0.tgz
  ci_cosign $repo/$chart_name:0.0.0
}

function ci_helm_fix_release() {
  local release="${1:-}"
  if [[ -z "$release" ]]; then
    echo "[error] Release name must be set."
    return 1
  fi

  local json=$(helm list --pending --no-headers --output=json -l name=$release)
  if [[ $(echo $json | jq '[ .[] ] | length == 1') == "true" ]]; then
    echo "[info] Found a pending release. Cleaning it up now. $json"
    if [[ $(echo $json | jq '[ .[0].revision="1" ] | length == 1') == "true" ]]; then
      helm uninstall $release
    else
      helm rollback $release --cleanup-on-fail
    fi
  fi
}

# Tell Artifact Hub about the Helm Chart in the specified directory.
# The chart must contain an Artifact Hub specific metadata file called 'artifacthub-repo.yml'.
function ci_helm_artifacthub_verify() {
  local dir="${1:-./}"
  if [[ ! -f "${dir}artifacthub-repo.yml" ]]; then
    return 0
  fi

  local repo="${2:-}"
  local name="$(yq -r '.name' ${dir}Chart.yaml)"

  if [[ -v CI_REGISTRY_PASSWORD && -v CI_REGISTRY_USER ]]; then
    echo "[info] Permission workaround for Bug https://$CI_SERVER_HOST/gitlab-org/gitlab/-/issues/444268"
    oras login $CI_REGISTRY --username $CI_REGISTRY_USER --password $CI_REGISTRY_PASSWORD > /dev/null || echo "Login to $CI_REGISTRY failed"
  else
    oras login $CI_REGISTRY --username $CI_REGISTRY_USER --password $CI_JOB_TOKEN > /dev/null || echo "Login to $CI_REGISTRY failed"
  fi

  echo "[info] oras push --artifact-type application/vnd.cncf.artifacthub.config.v1+yaml $repo/$name:artifacthub.io ${dir}artifacthub-repo.yml:application/vnd.cncf.artifacthub.repository-metadata.layer.v1.yaml"
  oras push --artifact-type application/vnd.cncf.artifacthub.config.v1+yaml $repo/$name:artifacthub.io ${dir}artifacthub-repo.yml:application/vnd.cncf.artifacthub.repository-metadata.layer.v1.yaml
}

# Build the chart and upload it either as a release or into the internal repository.
# If it is a release and the chart contains relevant metadata, then tell Artifact Hub about it.
function ci_helm_build_chart_and_push() {
  local dir="${1:-./}"
  local release="${2:-false}"
  echo "[info] Login to Kubernetes for Package validation"
  ci_kubernetes_config
  ci_helm_token_login
  echo "[info] Chart directory is $1"

  # Build and push the chart for the release
  if [[ -v CI_RELEASE_REGISTRY && -v CI_RELEASE_REPOSITORY && "$( echo "$CI_COMMIT_TAG" | grep -E -i ${CI_PATTERN_SEMVER})" != "" && "$release" == "true" ]]; then
    echo "[info] Build the chart and upload it into the release repository."
    repo="${CI_RELEASE_REGISTRY}/${CI_RELEASE_REPOSITORY}/charts"
    ci_helm_build_chart ${dir}
    ci_helm_artifacthub_verify ${dir} ${repo}
    ci_helm_upload_chart ${repo}
  fi

  echo "[info] Build the chart and upload it into the internal repository."
  export CI_RELEASE_REGISTRY=$CI_REGISTRY
  export CI_RELEASE_REPOSITORY=$CI_REGISTRY_PROJECT_PATH
  ci_helm_build_chart ${dir}

  # @TODO only push when it is latest
  # ci_helm_push_latest release/chart/
  ci_helm_upload_chart ${CI_REGISTRY}/${CI_REGISTRY_PROJECT_PATH}/charts
  ci_helm_upload_legacy
}


# Copy a Helm Chart from the specified directory to release/chart and build it with
# current environment variables and up-to-date dependencies.
function ci_helm_build_chart() {
  local dir="${1:-./}"
  if [ ! -d "release/chart" ]; then
    mkdir -p release/chart;
  else
    rm -Rf release/chart;
    mkdir -p release/chart;
  fi

  if [[ -f "${dir}Chart.yaml" ]]; then
    cp -RL ${dir}* release/chart
  else
    echo "[error] Chart not found in ${dir}"
    return 1;
  fi

  echo "[info] Using Helm version: $(helm version --short)"
  ci_helm_parse_env release/chart/

  ci_helm_dependency_update release/chart/

  if [[ "$( ci_helm_get_chart_version release/chart/)" == "0.0.0" ]]; then
    echo "Add hash to semantic version";
    sed -i "s#^version:.*#version: $(ci_helm_get_release_tag)#g" release/chart/Chart.yaml;
  fi
  echo "[info] Chart version: $( ci_helm_get_chart_version release/chart/)"

  echo "[Info] Packaging the chart in directory release/chart/ into a chart archive"
  ( cd release/chart/ && helm package ./ --destination ../charts/ )

  if [[ -f "release/chart/tests/test.yaml" ]]; then
    local chart_name="$(yq -r '.name' release/chart/Chart.yaml)"
    echo "[info] Copying from release/chart/tests/test.yaml into release/charts/$chart_name.yaml"
    cp release/chart/tests/test.yaml release/charts/$chart_name.yaml
    ci_envsubst release/charts/$chart_name.yaml
  else
    echo "[info] Chart test.yaml not found in ${dir}tests/test.yaml"
  fi
}

# Create a namespace in Kubernetes named after CI_ENVIRONMENT_SLUG.
function ci_helm_test_create_namespace() {
  if [[ -v CI_ENVIRONMENT_SLUG ]]; then
    local namespace="$CI_ENVIRONMENT_SLUG"
  else
    echo "[error] CI_ENVIRONMENT_SLUG is not set, cannot create namespace."
    exit 1
  fi

  ci_kubernetes_cleanup

  echo "[info] Wait till namespace $namespace is deleted from previous runs."
  kubectl delete namespace $namespace --wait=true --timeout=600s --now=true --ignore-not-found=true

  echo "[info] Create new Kubernetes namespace: $namespace"
  kubectl create namespace $namespace
  kubectl annotate namespace $namespace --overwrite "openshift.io/description=$CI_PROJECT_NAME ($CI_ENVIRONMENT_SLUG)"
  kubectl annotate namespace $namespace --overwrite "openshift.io/display-name=$CI_PROJECT_NAME ($CI_ENVIRONMENT_SLUG)"
  kubectl annotate namespace $namespace --overwrite xrow.com/cleanup=true
  kubectl config set-context --current --namespace=$namespace
  echo "[info] Namespace $namespace is ready for testing."
}

# Delete Kubernetes namespace named after CI_ENVIRONMENT_SLUG.
function ci_helm_test_delete_namespace() {
  if [[ -v CI_ENVIRONMENT_SLUG ]]; then
    local namespace="$CI_ENVIRONMENT_SLUG"
    kubectl config set-context --current --namespace=$namespace
  else
    echo "[error] CI_ENVIRONMENT_SLUG is not set, cannot delete namespace."
    exit 1
  fi

  for release in $( helm list --all -q -n $namespace | xargs -L1 )
  do
    echo "[info] Uninstall release '$release'."
    helm uninstall $release --no-hooks --wait || true
  done

  echo "[info] Wait until namespace '$namespace' is deleted to protect future runs."
  kubectl delete namespace $namespace --wait=true --timeout=600s --now=true --ignore-not-found=true
  killall kubectl && echo "Killed the watcher" || true
}

function ci_helm_test_chart() {
  local dir="${1:-./}"
  local release="${2:-release-$CI_JOB_ID}"
  echo "Helm version: $(helm version --short)"
  echo "Helm release: $release"
  echo "Helm timeout: $HELM_TIMEOUT"

  if [[ ! -d "release/charts/" || "$(ls -A release/charts/)" == "" ]]; then
    echo "[error] There are no charts in release/charts/ please provide it.";
    exit 1;
  fi

  ci_helm_test_create_namespace
  echo "Start watching events:"
  kubectl get events --watch &

  ci_helm_parse_env release/charts/

  if [[ "$(find release/charts/ -maxdepth 1 -regextype posix-extended -iregex '.*-crd.*[.]tgz' | sort -g)" != "" ]] ; then
    echo "Found CRD chart, it will get installed."
    file="$(find release/charts/ -maxdepth 1 -regextype posix-extended -iregex '.*-crd.*[.]tgz' | sort -g)"
    set +e
    helm upgrade $release-crd $file --debug --install --atomic --cleanup-on-fail --timeout $HELM_TIMEOUT
    RETURN_INSTALL=$?
    set -e
    if [[ $RETURN_INSTALL != 0  ]] ; then
      mkdir -p release/logs || true
      helm template $release $file $HELM_OPTS >> release/logs/$file.log
      echo "[error] Test failed, see release/logs/$file.log";
      ci_helm_test_delete_namespace
      exit 1;
    fi
    rm -Rf $(find release/charts/ -maxdepth 1 -regextype posix-extended -iregex '.*-crd.*[.]tgz' | sort -g)
  fi

  for file in $(find release/charts/ -regextype posix-extended -iregex '.*[.]tgz' | sort -g); do
    echo "[info] Found Helm Chart '$file'."
    unset BASH_REMATCH && [[ $(basename $file) =~ ([^.]+)-(${CI_PATTERN_SEMVER})(\.tgz) ]] || echo "[error] Error in regexp"
    chartname=${BASH_REMATCH[1]}
    version=${BASH_REMATCH[2]}

    testyaml="release/charts/$chartname.yaml"
    if [[ ! -f "$testyaml" ]] ; then
      echo "[info] File $testyaml does not exist, creating empty one."
      echo "---" > release/charts/$chartname.yaml
    fi
    export HELM_OPTS="--values $testyaml"

    ci_envsubst release/charts/$chartname.yaml

    echo "[info] Settings for project 'release/charts/$chartname.yaml':"
    cat release/charts/$chartname.yaml

    echo "[info] Start helm install:"
    echo helm install $release $file --debug --wait --atomic --timeout $HELM_TIMEOUT $HELM_OPTS
    set +e
    helm install $release $file --debug --wait --atomic --timeout $HELM_TIMEOUT $HELM_OPTS
    RETURN_INSTALL=$?
    set -e

    if [[ $RETURN_INSTALL == 0 ]] ; then
      echo "[info] Start helm upgrade:"
      echo helm upgrade $release $file --debug --wait --atomic --cleanup-on-fail --timeout $HELM_TIMEOUT $HELM_OPTS
      set +e
      helm upgrade $release $file --debug --wait --atomic --cleanup-on-fail --timeout $HELM_TIMEOUT $HELM_OPTS
      RETURN_UPGRADE=$?
      set -e
    fi

    if [[ $RETURN_INSTALL == 0 && $RETURN_UPGRADE == 0  ]] ; then
      echo "[info] Start helm test: helm test $release --timeout 25m0s"
      set +e
      helm test $release --timeout 25m0s
      RETURN_TEST=$?
      set -e
    fi

    if [[ $RETURN_INSTALL != 0 || $RETURN_UPGRADE != 0 || $RETURN_TEST != 0  ]] ; then
      mkdir -p release/logs || true
      helm template $release $file $HELM_OPTS > release/logs/$chartname.log
      echo "[error] Test failed, see release/logs/$chartname.log";
      ci_helm_test_delete_namespace
      exit 1;
    fi
  done

  echo "[info] Test completed with success";
  ci_helm_test_delete_namespace
  return 0
}

# Uninstall the specified release of the Helm Chart, delete jobs, and associated persistent volume claims
# Note: this operates on the current namespace in the current context, make sure you have set them first!
function ci_helm_remove_chart() {
  local release="${1}"
  local context="$(kubectl config current-context)"
  local namespace="$(kubectl config view --minify --output 'jsonpath={..namespace}')"

  echo "[info] Helm version: $(helm version --short)"
  echo "[info] Current Kubernetes context: $context"

  # This will uninstall the old version (no update)
  echo "$(date +[%T];) Uninstall release '$release' from namespace '$namespace'.";
  helm uninstall --wait --no-hooks --timeout=0s $release || true

  for i in $(kubectl get jobs | awk '{print $1}'); do
    kubectl delete jobs $i --now=true || true
  done

  echo "$(date +[%T];) Delete all persistent volume claims for release '$release' in namespace '$namespace'.";
  for i in $(kubectl get pvc --no-headers -l app.kubernetes.io/managed-by=Helm,app.kubernetes.io/instance=$release | awk '{print $1}'); do
    echo "[info] Deleting persistent volume claim: $i"
    kubectl delete pvc $i --now=true || true
  done

  echo "$(date +[%T];) Helm Chart removed.";
}

# Deploy a Helm Chart to the current Kubernetes context and namespace
function ci_helm_deploy_chart() {
  local release="${1:-$CI_PROJECT_NAME}"
  local config="${2:-deploy/$CI_ENVIRONMENT_SLUG.yaml}"
  local clean="${3:-false}"
  local chart

  if [ ! -d "release/charts/" ] ; then
    echo "[error] release/charts directory not found please configure it choose a different ci template";
    return 1;
  fi

  if [ -z "$(ls -A release/charts/)" ] ; then
    echo "[error] Chart not found in release/charts/ please provide it";
    return 1;
  fi

  chart="$(find release/charts/ -maxdepth 1 -regextype posix-extended -iregex '.*[.]tgz' | sort -g)"

  if [ ! -f "$config" ] ; then
    echo "[error] Helm Chart config file \"$config\" not found. Please provide it in deploy/*.yaml.";
    exit 1;
  fi

  echo "Helm version: $(helm version --short)"
  echo "Helm release name: $release"
  echo "Helm config: $config"
  echo "Helm chart: $chart"
  echo "Helm timeout: $HELM_TIMEOUT"
  echo "Helm cleanup: $clean"

  if [ "$clean" == 'true' ] ; then
    echo "[info] Cleanup application"
    ci_helm_remove_chart $release
    # @TODO Do we need it?
    # kubectl delete namespaces $namespace
  fi

  ci_helm_fix_release $release

  echo "[info] Parse environment variables in '$config' to generate settings for project in 'release/$release.yaml'."
  cp $config release/$release.yaml
  ci_envsubst release/$release.yaml

  echo "[info] Settings for project 'release/$release.yaml':"
  cat release/$release.yaml

  echo "[info] Rendered template for project '$chart' using settings in 'release/$release.yaml':"
  helm template $release $chart --values release/$release.yaml

  echo "[info] Deploying chart '$chart' by running 'helm upgrade' command."
  echo helm upgrade $release $chart --atomic --timeout ${HELM_TIMEOUT} --install --debug --skip-crds=true --values release/$release.yaml
  helm upgrade $release $chart --atomic --timeout ${HELM_TIMEOUT} --install --debug --skip-crds=true --values release/$release.yaml

  # For a clean deployment run jobs annotated with the 'install' pipeline hook
  if [ "$clean" == 'true' ] ; then
    ci_helm_run_hooks "install"
  fi

  echo "[info] Deployment completed"
}

# Run jobs with 'pipeline.xrow.com/hook' annotations in their metadata.
# The jobs should have been created by Helm in a suspended state and can be started
# by disabling the suspend status.
function ci_helm_run_hooks() {
  local name="${1:-}"
  local timeout="${2:-15m}"
  local hooks

  echo "[info] Looking for '$name' hooks to run."
  echo "[info] Note: it's the responsibility of the Helm Chart to pre-install any required jobs with a 'pipeline.xrow.com/hook' annotation."
  hooks=$(kubectl get jobs -o 'jsonpath={.items[?(@.metadata.annotations.pipeline\.xrow\.com/hook=="'"${name}"'")].metadata.name}' )

  for hook in $hooks; do
    kubectl patch "job/${hook}" --type=strategic --patch '{"spec":{"suspend":false}}'
    echo "Waiting for pipeline hook '$hook' job to start."
    kubectl wait --for=jsonpath='{.status.ready}'=1 "job/${hook}" || true
    kubectl logs "job/${hook}" --all-containers --follow
    echo
  done

  echo "[info] All pipeline hook '$name' jobs have run."
}

# Upload Helm Chart to the specified repository
function ci_helm_upload_chart() {
  local repo="${1:-}"

  if [[ ! -d "release/charts/" ]] ; then
    echo "[error] release/charts/ directory not found please configure it or choose a different ci template";
    exit 1;
  fi

  echo "[info] Using Helm version: $(helm version --short)"

  for file in $(find release/charts/ -regextype posix-extended -iregex '.*[.]tgz' | sort -g); do
    unset BASH_REMATCH && [[ $(basename $file) =~ ([^.]+)-(${CI_PATTERN_SEMVER})(\.tgz) ]] || echo "[error] Error in regexp"
    chartname=${BASH_REMATCH[1]}
    version=${BASH_REMATCH[2]}
    echo "[info] Push Helm Chart oci://$repo/$chartname:$version"
    echo helm push $file oci://$repo
    helm push $file oci://$repo
    ci_cosign $repo/$chartname:$version

    # Workaround for a strange bug where the pushed chart doesn't contain all the files.
    # Test it now to avoid wasting time later.
    local temp=$(mktemp -d -t test-XXXXXXXX)
    echo "[info] Test by pulling chart $chartname in version '$version'."
    helm pull oci://$repo/$chartname --version=$version --destination ${temp}
    local archive=$(find ${temp} -iname *.tgz)
    echo "Chart $archive was successfully pulled and stored in ${temp}."
    echo "[info] Test by linting the Helm Chart Archive $archive."
    helm lint $archive
    if [ $? != 0 ] ; then
      echo "[error] Linting $file failed. That's strange, try running this job again."
      exit 1
    fi

    echo "[info] Linting passed."
  done
}

function ci_helm_upload_legacy() {
  if [[ -v CI_COMMIT_TAG && "$( echo "$CI_COMMIT_TAG" | grep -E -i ${CI_PATTERN_SEMVER})" != "" ]] ; then
    repotype="stable"
  else
    repotype="testing"
  fi

  for file in $(find release/charts/ -regextype posix-extended -iregex '.*[.]tgz' | sort -g); do
    unset BASH_REMATCH && [[ $(basename $file) =~ ([^.]+)-(${CI_PATTERN_SEMVER})(\.tgz) ]] || echo "[error] Error in regexp"
    chartname=${BASH_REMATCH[1]}
    version=${BASH_REMATCH[2]}
    # Project
    repourl="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/${repotype}"
    repoapi="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/${repotype}/charts"
    echo "Push to project helm repository $repourl"
    curl --request POST --form 'chart=@'${file} --user ${CI_REGISTRY_USER}:${CI_JOB_TOKEN} $repoapi
    echo "\n"

    # Shared
    if [[ $( echo "$CI_PROJECT_NAMESPACE" | grep -P "xrow-shared" ) ]] ; then
      repourl="${CI_API_V4_URL}/projects/${CI_SHARED_REPOID}/packages/helm/${repotype}"
      repoapi="${CI_API_V4_URL}/projects/${CI_SHARED_REPOID}/packages/helm/api/${repotype}/charts"
      echo "Push to shared helm repository $repourl"
      curl --request POST --form 'chart=@'${file} --user ${CI_REGISTRY_USER}:${CI_JOB_TOKEN} $repoapi
      echo "\n"
    fi
  done
}

# Set the value of the variable 'chart_version' according to the supplied value.
# If the supplied value is '' then use the current Git commit tag or hash.
function ci_helm_get_release_tag() {
  local chart_version="0.0.0"
  if [[ -v CI_COMMIT_TAG  ]]; then
    chart_version=${CI_COMMIT_TAG}
  elif [[ -v CI_COMMIT_SHA ]]; then
    local branch
    if [[ -v CI_MERGE_REQUEST_TARGET_BRANCH_NAME ]]; then
      branch=$( sed -E 's/[\/]/./g;' <<< ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME} )
    else
      branch=$( sed -E 's/[\/]/./g;' <<< ${CI_COMMIT_BRANCH} )
    fi
    chart_version="0.0.0+${branch}.${CI_COMMIT_SHA}"
  fi
  echo "$chart_version"
  return 0
}

function ci_helm_get_chart_version() {
  local chart_ref="${1:-./chart/}"
  local chart_version=""
  if [[ -f "${chart_ref}Chart.yaml" ]]; then
    chart_version="$(yq -r '.version' ${chart_ref}Chart.yaml)"
  else
    chart_version=$( sed -E 's/(.*):(.*)/\2/g;' <<< $chart_ref )
  fi
  echo "$chart_version"
  return 0
}

# Set the value of the variable 'chart_name' according to the supplied value.
function ci_helm_get_chart_name() {
  local chart_ref="${1:-./chart/}"
  local chart_name=""
  if [[ -f "${chart_ref}Chart.yaml" ]]; then
    chart_name="$(yq -r '.name' ${chart_ref}Chart.yaml)"
  else
    chart_name=$( sed -E 's/(.*):(.*)/\1/g;' <<< $chart_ref )
  fi
  echo $chart_name
  return 0
}

# pull a chart and store it in folder release/charts
function ci_helm_get_chart() {
  ci_helm_token_login
  echo "function ci_helm_get_chart:"
  echo "Usage A: CHART=oci://registry/repo/chartname:0.0.0"
  echo "Usage B: CHART=xrow-stable/chartname:0.0.0 or CHART=xrow-stable/chartname"
  echo "Usage C: CHART=path/to/filename";

  if [ ! -v 1 ]; then
    echo "[error] Parameter CHART might not have been set."
    exit 1;
  fi

  local chart_version=$( ci_helm_get_chart_version ${1} )
  local chart_name=$( ci_helm_get_chart_name ${1} )

  mkdir -p release/charts || true
  echo "[info] Using Helm version: $(helm version --short)"
  echo "[info] Pulling chart $chart_name in version '$chart_version'"
  helm pull $chart_name --version=$chart_version --destination release/charts
  local chart_file=$(find ./release/charts -iname *.tgz)
  echo "Chart $chart_file was successfully pulled and stored in release/charts."
  return 0
}

# Lint the Helm Chart in 'release/charts' using the specified configuration YAML file in the 'deploy' directory.
function ci_helm_lint_config() {
  for file in $(find release/charts/ -regextype posix-extended -iregex '.*[.]tgz' | sort -g); do
    echo "[info] Found Helm Chart $file"
  done

  local linted=0
  for config in $(find deploy/ -type f -regextype posix-extended -iregex "deploy/${1}[.]ya?ml" | sort -g); do
    echo "[info] Linting Helm Chart $file using values in $config"
    linted=1
    helm lint $file --values $config
    RETURN=$?
    if [ $RETURN != 0 ] ; then
      echo "[error] Linting $file with values in $config failed with status '$RETURN'."
    fi
  done

  if [ $linted == 0 ] ; then
    echo "[error] Didn't find configuration '${1}'"; exit 1
  fi
}

# Login to Helm Chart registries using credentials stored in environment variables
function ci_helm_token_login() {
  if [[ -v RH_REGISTRY_ACCOUNT && -v RH_REGISTRY_PASSWORD ]]; then
    echo "${RH_REGISTRY_PASSWORD}" | helm registry login -u ${RH_REGISTRY_ACCOUNT} --password-stdin registry.redhat.io > /dev/null || echo "[warn] Login to registry registry.redhat.io failed"
  elif [[ -v RH_USERNAME && -v RH_PASSWORD ]]; then
    echo "${RH_PASSWORD}" | helm registry login -u ${RH_USERNAME} --password-stdin registry.redhat.io > /dev/null || echo "[warn] Login to registry registry.redhat.io failed"
  else
    echo "RH_REGISTRY_ACCOUNT is either not set or it is protected. The Helm repository at registry.redhat.io will be not available."
  fi

  if [[ -v CI_REGISTRY_PASSWORD && -v CI_REGISTRY_USER ]]; then
    echo "[info] Permission workaround for bug https://gitlab.com/gitlab-org/gitlab/-/issues/444268"
    echo "${CI_REGISTRY_PASSWORD}" | helm registry login -u ${CI_REGISTRY_USER} --password-stdin $CI_REGISTRY > /dev/null || echo "[warn] Login to registry $CI_REGISTRY failed"
  else
    echo "${CI_JOB_TOKEN}" | helm registry login -u ${CI_REGISTRY_USER} --password-stdin $CI_REGISTRY > /dev/null || echo "[warn] Login to registry $CI_REGISTRY failed"
  fi

  if [[ -v CI_DEPLOY_PASSWORD ]]; then
    echo "[info] Adding Helm repositories from $CI_SHARED_REPO_URL via CI_DEPLOY_PASSWORD..."
    # @TODO Note: CI_JOB_TOKEN will not work.
    helm repo add --username ${CI_DEPLOY_USER} --password ${CI_DEPLOY_PASSWORD} xrow-testing https://$CI_SERVER_HOST/api/v4/projects/${CI_SHARED_REPOID}/packages/helm/testing || true
    helm repo add --username ${CI_DEPLOY_USER} --password ${CI_DEPLOY_PASSWORD} xrow-stable https://$CI_SERVER_HOST/api/v4/projects/${CI_SHARED_REPOID}/packages/helm/stable || true
  else
    echo "CI_DEPLOY_PASSWORD is either not set or it is protected. Helm repositories from $CI_SHARED_REPO_URL will be not available."
  fi

  if [[ -v HELM_REGISTRY_PASSWORD ]]; then
    echo "[info] Adding Helm repositories from $CI_SHARED_REPO_URL via HELM_REGISTRY_PASSWORD..."
    #echo "1. xrow-testing -> https://$CI_SERVER_HOST/api/v4/projects/${CI_SHARED_REPOID}/packages/helm/testing"
    #echo "2. xrow-stable -> https://$CI_SERVER_HOST/api/v4/projects/${CI_SHARED_REPOID}/packages/helm/stable"
    helm repo add --username $HELM_REGISTRY_USER --password $HELM_REGISTRY_PASSWORD xrow-testing https://$CI_SERVER_HOST/api/v4/projects/${CI_SHARED_REPOID}/packages/helm/testing || true
    helm repo add --username $HELM_REGISTRY_USER --password $HELM_REGISTRY_PASSWORD xrow-stable https://$CI_SERVER_HOST/api/v4/projects/${CI_SHARED_REPOID}/packages/helm/stable || true
  elif [[ -v CI_JOB_TOKEN ]]; then
    echo "[info] Adding Helm repositories from $CI_SHARED_REPO_URL via CI_JOB_TOKEN..."
    # echo "1. xrow-testing -> https://$CI_SERVER_HOST/api/v4/projects/${CI_SHARED_REPOID}/packages/helm/testing"
    # echo "2. xrow-stable -> https://$CI_SERVER_HOST/api/v4/projects/${CI_SHARED_REPOID}/packages/helm/stable"
    helm repo add --username ${CI_REGISTRY_USER} --password $CI_JOB_TOKEN xrow-testing https://$CI_SERVER_HOST/api/v4/projects/${CI_SHARED_REPOID}/packages/helm/testing || true
    helm repo add --username ${CI_REGISTRY_USER} --password $CI_JOB_TOKEN xrow-stable https://$CI_SERVER_HOST/api/v4/projects/${CI_SHARED_REPOID}/packages/helm/stable || true
  fi
}

# Pull down the latest charts to satisfy the dependencies of all the charts in
# the specified sub-tree.
function ci_helm_dependency_update() {
    local dir="${1:-./}"
    local chartyaml

    for chartyaml in $( find ${dir} -name "Chart.yaml" | sort -g -r ); do
      echo "[info] Update dependencies for chart: $(dirname ${chartyaml})";
      helm dependency update $(dirname ${chartyaml})
    done
}
function ci_helm_dependency_build() {
    local dir="${1:-./}"
    local chartyaml

    for chartyaml in $( find ${dir} -name "Chart.yaml" | sort -g -r ); do
      echo "[info] Update and build dependencies for chart: $(dirname ${chartyaml})";
      helm dependency update $(dirname ${chartyaml})
      helm dependency build $(dirname ${chartyaml})
    done
}
function ci_helm_parse_env() {
    local dir="${1:-./}"
    local chartyaml
    local file

    for chartyaml in $( find "${dir}" -maxdepth 3 -name "Chart.yaml" | sort -g -r ); do
      echo "[info] Parse environment variables in $(dirname ${chartyaml})"
      for file in $(find "$(dirname ${chartyaml})" -maxdepth 1 -regextype posix-extended -iregex '.*[.]y[a]?ml' | sort -g); do
        ci_envsubst "${file}"
      done
      ci_markdown_parse_env "$(dirname ${chartyaml})"/
    done
}

# Lint the Helm Chart in the specified directory (or current directory if not supplied)
# If a file 'tests/test.yaml' exists then use it for setting chart values.
function ci_helm_lint() {
  local dir="${1:-./}"
  ci_helm_install
  helm version --template='[info] HELM version: {{.Version}}'

  local helm_opts=""
  if [[ -f ${dir}tests/test.yaml ]]; then
    helm_opts=" --values ${dir}tests/test.yaml"
  fi

  ci_helm_token_login
  ci_helm_dependency_update ${dir}
  ci_helm_parse_env ${dir}
  echo "[info] Linting Chart: helm lint ${dir} $helm_opts";
  echo "@TODO helm lint --with-subcharts ${dir} $helm_opts broken due https://github.com/helm/helm/issues/12798 "
  set +e
  helm lint ${dir} $helm_opts
  RETURN=$?
  set -e

  if [ $RETURN == 0 ] ; then
    echo "[info] Test completed with success";
  else
    echo "[error] Test failed with status '$RETURN'"; exit 1;
  fi
}

# Pull the specified chart into the specified directory, with retry.
ci_helm_pull_chart_wait() {
  local dir="${1}"
  local chart="${2}"
  local timeout="${3:-600}"
  local sleep_interval="${3:-30}"
  local chart_version=$( ci_helm_get_chart_version ${2} )
  local chart_name=$( ci_helm_get_chart_name ${2} )
  local start_time=$(date +%s)

  while true; do
    if helm pull oci://$chart_name --version=$chart_version --untardir ${dir}}helm-charts/; then
      echo "Chart pulled successfully."
      break
    else
      echo "Chart not found, retrying in $sleep_interval seconds..."
      sleep $sleep_interval
      local current_time=$(date +%s)
      if (( current_time - start_time >= $timeout )); then
        echo "Timeout reached, stopping..."
        return 1
        break
      fi
    fi
  done
  return 0
}
