#!/bin/bash

# Delete any Kubernetes namespaces from the current context that have the 'xrow.com/cleanup' annotation set to true.
function ci_kubernetes_cleanup() {
    for oldnamespace in $( kubectl get namespaces -A -o json | jq -r '[.items][][] | select( .metadata.creationTimestamp | fromdateiso8601 < ( now - 1*3600 ) ) | select( .metadata.annotations."xrow.com/cleanup" == "true" ) | .metadata.name ' )
    do
        kubectl delete namespace $oldnamespace --wait=false --now=true --ignore-not-found=true
    done
}

function ci_kubernetes_minikube() {
  curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm
  rpm -Uvh minikube-latest.x86_64.rpm
  rm -Rf minikube-latest.x86_64.rpm
  yum install -y conntrack
  minikube start --force
  kubectl describe node
  kubectl get all --all-namespaces
}

function ci_kubernetes_kind() {
  #export KIND_EXPERIMENTAL_PROVIDER=podman
  KIND_VERSION="${KIND_VERSION:-0.24.0}"
  KUBERNETES_VERSION="${KUBERNETES_VERSION:-1.30.4}"
  dnf install -y go
  echo "Preparing Kind..."
  export PATH="$(go env GOPATH)/bin:$PATH"
  if [[ ! -v XDG_RUNTIME_DIR ]]; then
      export XDG_RUNTIME_DIR="/run/user/$(id -u)"
  fi
  go install sigs.k8s.io/kind@v${KIND_VERSION}
  kind delete cluster
  sleep 1
  sysctl net.ipv6.conf.all.disable_ipv6=0
  kind create cluster --image kindest/node:v${KUBERNETES_VERSION}
  kubectl cluster-info --context kind-kind
  local namespace="kind-kind"
  echo "Namespace: $namespace"
  # kubectl patch deployment coredns -n kube-system --patch-file $TOOLS_DIR/scripts/files/kind/dns.yaml
  # kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.11.0/cert-manager.yaml
  kubectl describe node
  kubectl get all --all-namespaces
}

function ci_kubernetes_connect_local() {
  if [[ -f /run/secrets/kubernetes.io/serviceaccount/token ]]; then
    export KUBECONFIG=$(mktemp -d -p /tmp)/config
    touch ${KUBECONFIG}
    chmod 600 ${KUBECONFIG}
    kubectl config set-cluster default --server=https://kubernetes.default.svc --certificate-authority=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt;
    kubectl config set-context default --cluster=default;
    kubectl config set-credentials user --token=$(cat /run/secrets/kubernetes.io/serviceaccount/token);
    kubectl config set-context default --user=user;
    kubectl config use-context default;
    echo "Created kubectl config in ${KUBECONFIG}";
  else
    echo "Gitlab Runner is no Kubernetes GitLab runner. No credentials (/run/secrets/kubernetes.io/serviceaccount/token) for Kubernets are provided."
    exit 1
  fi
}

function ci_kubernetes_create_namespace() {
  local namespace="${1}"
  echo "Project: $CI_PROJECT_NAME ($CI_ENVIRONMENT_SLUG)"
  echo "Environment Name: $CI_ENVIRONMENT_NAME"
  echo "Environment Slug: $CI_ENVIRONMENT_SLUG"
  echo "Namespace: $namespace"
  kubectl create namespace $namespace > /dev/null && echo "Namespace $namespace created" || echo "Namespace $namespace exists"
  kubectl config set-context --current --namespace=$namespace;
}

function ci_kubernetes_config_token() {
  echo "${KUBERNETES_TOKEN}" | tr -d ' ' | base64 -d > /tmp/serviceaccount.secret
  cat /tmp/serviceaccount.secret | jq -r '.data["ca.crt"]' | base64 -d > /tmp/ca.crt
  mkdir -p ~/.kube
  kubectl config set-cluster cfc --server=${KUBERNETES_API} --embed-certs=true --certificate-authority=/tmp/ca.crt
  kubectl config set-context cfc --cluster=cfc --namespace=${KUBERNETES_NAMESPACE}
  kubectl config set-credentials user --token=$(cat /tmp/serviceaccount.secret | jq -r '.data["token"]'| base64 -d)
  kubectl config set-context cfc --user=user
  kubectl config use-context cfc

  sudo mkdir -p /root/.kube
  sudo kubectl config set-cluster cfc --server=${KUBERNETES_API} --embed-certs=true --certificate-authority=/tmp/ca.crt
  sudo kubectl config set-context cfc --cluster=cfc --namespace=${KUBERNETES_NAMESPACE}
  sudo kubectl config set-credentials user --token=$(cat /tmp/serviceaccount.secret | jq -r '.data["token"]'| base64 -d)
  sudo kubectl config set-context cfc --user=user
  sudo kubectl config use-context cfc
}

# Connect a remote kubernetes port to localhost
# ci_kubernetes_port_forward $SRC_NAMESPACE $POD $SRC_PORT 3306
function ci_kubernetes_port_forward() {
  echo "[Info] Forwarding port $4 on pod $2 in namespace ${1} to port $3 on localhost..."
  kubectl port-forward pod/${2} ${3}:${4} --address=0.0.0.0 -n ${1} ${5} &
  sleep 3;
  timeout 30 bash -c "until echo > /dev/tcp/0.0.0.0/${3} > /dev/null; do sleep 3; done"
}

# Kill all running 'kubectl' processes (probably port-forwarding processes).
function ci_kubernetes_kill {
  echo "[info] Kill all existing port-forwarding connections."
  killall kubectl || true
}

# Set Kubernetes configuration, context and namespace.
function ci_kubernetes_config() {
  local context="${1:-$DST_CONTEXT}"
  local namespace="${2:-$DST_NAMESPACE}"

  if [[ -z $context ]]; then
    echo "[info] Context is not set"
  fi;

  if [[ -z "$namespace" ]]; then
    echo "[info] Namespace is not set"
  fi;

  if [[ -f "${KUBECONFIG}" ]]; then
    tmpfile=$(mktemp -d -p /tmp)/config
    cp ${KUBECONFIG} $tmpfile
    chmod 400 $tmpfile
    export KUBECONFIG=$tmpfile
  fi

  if [[ -v KUBECONFIG_CONTENT && ! -f "${KUBECONFIG}" ]]; then
    export KUBECONFIG
    KUBECONFIG="$(echo ~/.kube/config)";
    mkdir -p $(dirname "${KUBECONFIG}")
    # Is base64 string?
    if [[ "$( echo ${KUBECONFIG_CONTENT} | tr -d ' ' | base64 -d | yq -r .kind )" == "Config" ]]; then
      echo "${KUBECONFIG_CONTENT}" | tr -d ' ' | base64 -d > ${KUBECONFIG}
      chmod 400 $KUBECONFIG
    else
      echo "${KUBECONFIG_CONTENT}" > ${KUBECONFIG}
    fi
    echo "[info] KUBECONFIG_CONTENT used for configuration for ${KUBECONFIG}";
  fi

  if [[ ! -f "$KUBECONFIG" ]]; then
    echo "[warning] $KUBECONFIG missing";
  fi

  if [[ -n "$context" ]]; then
    kubectl config use-context $context;
  fi

  if [[ -n "$namespace" ]]; then
    kubectl config set-context --current --namespace=$namespace;
  fi
}

# Syntax: ci_kubernetes_create_service_account <namespace> <service-account-name>
# Example: ci_kubernetes_create_service_account ibexa-production deployment-user
function ci_kubernetes_create_service_account() {
  files="${TOOLS_DIR}/scripts/files/kubernetes/create_service_account/"
  export NAMESPACE=${1}
  export USER=${2}
  rm -Rf kubeconfig-token
  rm -Rf kubeconfig-client-cert
  rm -Rf kube.key
  rm -Rf kube.csr
  rm -Rf kube.crt
  rm -Rf csr.cnf

  kubectl apply -f ${files}rbac.yaml
  kubectl apply -f ${files}rbac-admin.yaml

  openssl genrsa -out kube.key 4096
  cat ${files}csr.cnf.tpl | envsubst > csr.cnf
  openssl req -config csr.cnf -new -key kube.key -nodes -out kube.csr
  kubectl delete csr/new-csr
  export BASE64_CSR=$(cat ./kube.csr | base64 | tr -d '\n')
  cat csr.yaml | envsubst | kubectl apply -f -

  kubectl certificate approve new-csr
  kubectl get csr new-csr -o jsonpath='{.status.certificate}' | base64 --decode > kube.crt
  oc create sa ${USER} -n kube-public
  oc adm policy add-cluster-role-to-user self-provisioner -z ${USER} -n kube-public
  oc adm policy add-cluster-role-to-user xrow-self-provisioner-with-namespaces -z ${USER} -n kube-public

  if [ -v NAMESPACE ] ; then
    kubectl create namespace ${NAMESPACE} || true
    oc policy add-role-to-user admin system:serviceaccount:kube-public:${USER} -n ${NAMESPACE}
  else
    NAMESPACE="kube-public"
  fi
  kubectl config set-credentials ${USER} --client-key=kube.key --embed-certs=true
  export CURRENT_CONTEXT=$(kubectl config current-context)
  export CLUSTER_NAME=$(kubectl config view --raw -o json | jq -r '.contexts[] | select(.name == "'$(kubectl config current-context)'") | .context.cluster ')
  export CLIENT_CERTIFICATE_DATA=$(kubectl get csr new-csr -o jsonpath='{.status.certificate}')
  export CLUSTER_CA=$(kubectl config view --raw -o json | jq -r '.clusters[] | select(.name == "'$CLUSTER_NAME'") | .cluster."certificate-authority-data"')
  export CLUSTER_ENDPOINT=$(kubectl config view --raw -o json | jq -r '.clusters[] | select(.name == "'$CLUSTER_NAME'") | .cluster."server"')
  export TOKEN_NAME=$(kubectl get serviceaccounts ${USER} -n kube-public -o json | jq -r '.secrets[] | select(.name|test("-token-")) | .name')
  export TOKEN=$(kubectl get secret ${TOKEN_NAME} -n kube-public -o json | jq -r '.data.token' | base64 -d)
  cat ${files}config-client-cert.tpl | envsubst > kubeconfig-client-cert
  kubectl config set-credentials ${USER} \
    --client-certificate=$PWD/kube.crt \
    --client-key=$PWD/kube.key \
    --embed-certs=true \
    --kubeconfig=$PWD/kubeconfig
  cat ${files}config-token.tpl | envsubst > kubeconfig-token
  rm -Rf kube.key
  rm -Rf kube.csr
  rm -Rf kube.crt
  chmod 600 $PWD/kubeconfig-*
  kubectl --kubeconfig=$PWD/kubeconfig-token get pods -n ${NAMESPACE}

  echo "[info] kubeconfig created in $PWD/kubeconfig-token"
}

# Delete named pod and configmap from a namespace.
function ci_kubernetes_delete_pod() {
  if [ -z "$1" ]; then
    echo "[error] Missing namespace."
    exit 1
  fi

  if [ -z "$2" ]; then
    echo "[error] Missing pod name."
    exit 1
  fi

  echo "[info] Delete pod '${2}' from namespace '${1}' (ignore errors)."
  kubectl delete pod/${2} -n ${1} || true
  kubectl delete configmap/${2} -n ${1} || true
}
