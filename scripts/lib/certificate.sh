#!/bin/bash

function ci_import_letsencrypt_staging {
  # RHEL
  if [[ -d "/etc/pki/ca-trust/source/anchors" ]]; then
    curl -o /etc/pki/ca-trust/source/anchors/letsencrypt-stg-root-x1.crt -s https://letsencrypt.org/certs/staging/letsencrypt-stg-root-x1.pem
    curl -o /etc/pki/ca-trust/source/anchors/letsencrypt-stg-root-x2.crt -s https://letsencrypt.org/certs/staging/letsencrypt-stg-root-x2.pem
    update-ca-trust
  fi
  # Debian
  if [[ -d "/usr/local/share/ca-certificates" ]]; then
    curl -o /usr/local/share/ca-certificates/letsencrypt-stg-root-x1.crt -s https://letsencrypt.org/certs/staging/letsencrypt-stg-root-x1.pem
    curl -o /usr/local/share/ca-certificates/letsencrypt-stg-root-x2.crt -s https://letsencrypt.org/certs/staging/letsencrypt-stg-root-x2.pem
    update-ca-certificates
  fi
}