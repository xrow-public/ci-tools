#!/bin/bash

function ci_hvs_init {
  ci_env_require HCP_CLIENT_ID
  ci_env_require HCP_CLIENT_SECRET
  ci_env_require HCP_ORGANIZATION
  ci_env_require HCP_PROJECT
  ci_env_require HCP_APPLICATION

  if [[ ! -x "/usr/bin/vlt" ]]; then
    curl -fsSL --output /etc/yum.repos.d/hashicorp.repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
    yum install vlt -y
  fi
  vlt login
  vlt config init --non-interactive --organization ${HCP_ORGANIZATION} --project ${HCP_PROJECT} --app-name ${HCP_APPLICATION}
}

function ci_hvs_load_env {
  ci_hvs_init
  HCP_ENV_KEYNAME=${1}
  ci_env_require HCP_ENV_KEYNAME
  local contents=$(vlt secrets get -plaintext $HCP_ENV_KEYNAME| base64 -d )
  if [[ ! -z $contents ]]; then
    echo "[info] Loading env from vault ${HCP_APPLICATION}"
    set -o allexport # enable all variable definitions to be exported
    source <( echo "${contents}" | sed -e "s/\r//" -e "s/'/'\\\''/g" )
    set +o allexport
  fi
}
