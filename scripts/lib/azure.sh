#!/bin/bash

if [[ -v AGENT_NAME ]]; then
  echo "[info] Azure will not set CI_COMMIT_TAG. Set env CI_COMMIT_TAG, if you on a tagged commit." 
  if [[ ! -v CI_COMMIT_AUTHOR ]]; then
    CI_COMMIT_AUTHOR="${BUILD_SOURCEVERSIONAUTHOR} <${CI_COMMIT_EMAIL}>"
  fi
  if [[ ! -v CI_PROJECT_NAME ]]; then
    CI_PROJECT_NAME="${BUILD_REPOSITORY_NAME}"
  fi
  if [[ ! -v CI_PROJECT_ID ]]; then
    CI_PROJECT_ID="${BUILD_REPOSITORY_ID}"
  fi
  if [[ ! -v CI_COMMIT_BRANCH ]]; then
    CI_COMMIT_BRANCH="${BUILD_SOURCEBRANCH}"
  fi
  if [[ ! -v CI_COMMIT_MESSAGE ]]; then
    CI_COMMIT_MESSAGE="${BUILD_SOURCEVERSIONMESSAGE}"
  fi
  if [[ ! -v CI_COMMIT_REF_SLUG ]]; then
    if [[ -v CI_COMMIT_TAG ]]; then
        CI_COMMIT_REF_SLUG="${CI_COMMIT_TAG}"
    else
        CI_COMMIT_REF_SLUG="${BUILD_SOURCEBRANCHNAME}"
    fi
  fi
  if [[ ! -v CI_COMMIT_SHA ]]; then
    CI_COMMIT_SHA="${BUILD_SOURCEVERSION}"
  fi
  if [[ ! -v CI_COMMIT_SHORT_SHA ]]; then
    # Using Legth 8 of the commit hash, because it is shown in the Azure DevOps UI
    CI_COMMIT_SHORT_SHA="$( echo -n "${BUILD_SOURCEVERSION}" | cut -c1-8 )"
  fi
  if [[ ! -v CI_PIPELINE_IID ]]; then
    CI_PIPELINE_IID="${BUILD_BUILDID}"
  fi
  export CONTAINER_RUNTIME="nerdctl"
  export CONTAINER_RUNTIME_PUSH="nerdctl push"
  export CONTAINER_BUILD="nerdctl build"
  # @TODO  
  # CI_COMMIT_TAG="${BUILD_SOURCEVERSION}"
  CI_PIPELINE_IID="${BUILD_BUILDID}"
fi

function ci_azure_init {
  local context="${1:-.}"
  export GIT_DIR=$context
  if [[ ! -x "azure-cli" ]]; then
    pipx install azure-cli 2>&1 >/dev/null
  fi
}

function ci_azure_login {
    ci_dotenv
    ci_azure_init
    if [[ ! -v AZURE_CLIENT_ID ]]; then
      echo "Missing AZURE_CLIENT_ID"
      exit 1
    fi
    if [[ ! -v AZURE_TENANT ]]; then
      echo "Missing AZURE_TENANT"
      exit 1
    fi
    if [[ ! -v AZURE_SUBSCRIPTION_ID ]]; then
      echo "Missing AZURE_SUBSCRIPTION_ID"
      exit 1
    fi
    if [[ ! -v AZURE_CLIENT_SECRET ]]; then
      echo "Missing AZURE_CLIENT_SECRET"
      exit 1
    fi
    az login --service-principal --username ${AZURE_CLIENT_ID} --tenant ${AZURE_TENANT} --password ${AZURE_CLIENT_SECRET} || exit 1
    az account set --subscription ${AZURE_SUBSCRIPTION_ID} 2>&1 >/dev/null
}

function ci_azure_init_kubeconfig {
    ci_dotenv
    local name="${2:-$CI_AZURE_CLUSTER_NAME}"
    local group="${1:-$CI_AZURE_GROUP}"
    if [[ ! -v CI_AZURE_CLUSTER_NAME ]]; then
      echo "Missing CI_AZURE_CLUSTER_NAME"
      exit 1
    fi
    if [[ ! -v group ]]; then
      echo "Missing CI_AZURE_GROUP"
      exit 1
    fi
    if [[ $(az aks list --query "[?name=='$name'].{Name:name, Power:powerState.code}" -o json ) =~ "Running" ]]; then
      echo "[info] Cluster started"
    else
      echo "[error] Cluster not started"
      return 1
    fi
    export KUBECONFIG=$(mktemp -d -p /tmp)/config
    az aks get-credentials -n ${name} -g ${CI_AZURE_GROUP} --admin --overwrite-existing --file ${KUBECONFIG}
    echo "[info] Created kubernetes config at $KUBECONFIG"
    return 0
}