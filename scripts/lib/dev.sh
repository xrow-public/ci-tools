#!/bin/bash

function ci_debug() {
  echo "[info] BASH_VERSION: ${BASH_VERSION}"
  echo "[info] Loaded Library to temp dir $TOOLS_DIR"
  echo "CWD: $(pwd)"
  echo "HOME: ${HOME}"
  echo "[info] env ist stored in /tmp/env. Reload via: source /tmp/env"
  export > /tmp/env
  set -x
  set -o pipefail
}

function ci_dev_env {
  export CI_COMMIT_TAG="0.0.0"
  export CI_REGISTRY="registry.gitlab.com"
  export CI_PROJECT_PATH="xrow-shared/helm-ezplatform"
  export IBEXA_PACKAGE="experience"
  export IBEXA_VERSION="4.6.1"
  export CI_PROJECT_NAME="helm-ezplatform"
  export CI_PIPELINE_IID="1010"
  export CI_JOB_ID="1010"
  export CI_ENVIRONMENT_SLUG="test"
  export RELEASE="ibexa"
  export CI_COMMIT_REF_NAME="$(git rev-parse --abbrev-ref HEAD)"
  export CI_COMMIT_REF_SLUG="$(git rev-parse --abbrev-ref HEAD)"
  export CI_COMMIT_SHA="$(git rev-parse HEAD)"
  export CI_DEFAULT_BRANCH="master"
}

function ci_dev_local_setup_centos {
  composer create-project netgen/media-site
  cd media-site
  yarn install
  yarn build:prod
  echo 'SERVER_ENVIRONMENT=local' >> ./env.local
  echo 'DATABASE_URL="mysql://127.0.0.1:3306/mediasite?serverVersion=8&charset=utf8mb4"' > ./env.local
  echo 'MAILER_DSN="smtp://0.0.0.0:25"' > ./env.local
  php -d memory_limit=-1 bin/console ibexa:install --skip-indexing netgen-media
  php -d memory_limit=-1 bin/console ibexa:reindex --processes=0 -n
  php bin/console ibexa:graphql:generate-schema
}

function ci_dev_local_setup_centos_stream {
  dnf install http://rpms.remirepo.net/enterprise/remi-release-9.rpm
  dnf install -y dnf-utils
  dnf module reset -y php
  dnf module -y install php:remi-8.2
  yum install -y php php-fpm nss-tools
  yum install -y php-pecl-imagick php-gd
  yum install -y mysql-server
  yum install -y redis
  adduser default
  usermod -aG root default
  curl -sS https://get.symfony.com/cli/installer | bash
  rm -Rf /usr/local/bin/symfony
  mv /root/.symfony5/bin/symfony /usr/local/bin/symfony
  symfony server:ca:install
  curl -o- -L https://yarnpkg.com/install.sh | bash
  export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
  npm install -g sass
  sed -i "s/.*user.*=.*/user = root/g" /etc/php-fpm.d/www.conf
  sed -i "s/.*group.*=.*/group = root/g" /etc/php-fpm.d/www.conf
  sed -i "s/.*max_execution_time.*=.*/max_execution_time = 0/g" /etc/php.ini
  sed -i "s/.*memory_limit.*=.*/memory_limit = 2G/g" /etc/php.ini
  sed -i "s/.*ExecStart.*=.*/ExecStart=\/usr\/sbin\/php-fpm --nodaemonize --allow-to-run-as-root/g" /usr/lib/systemd/system/php-fpm.service
  systemctl daemon-reload
  systemctl restart php-fpm
  systemctl restart mysqld
  echo "default ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/symfony

  sudo -u default /usr/local/bin/symfony server:ca:install
  sudo -u default /usr/local/bin/symfony server:start --port 8080
  # podman run -it -u 0 -p 8080:8080	-v ./:/opt/app-root/src:z registry.gitlab.com/xrow-public/repository/php:latest bash /usr/libexec/s2i/run
}

function ci_dev_install_tools() {
  export HELM_INSTALL_DIR="/usr/bin"
  curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
  curl -o /usr/bin/kubectl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
  chmod 755 /usr/bin/kubectl
}