#!/bin/bash

function ci_mkdocs_changelog {
  local dir="${1:-docs}"

  if [[ -v GITLAB_TOKEN ]]; then
    echo "Generating Changelog"
    echo "@xrow-public:registry=https://$CI_SERVER_HOST/api/v4/packages/npm/" >> .npmrc
    echo 'Installing typescript...'
    npm install -g typescript
    echo 'Installing ts-node...'
    npm install -g ts-node
    echo 'Installing xrow-public/changelog...'
    npm install '@xrow-public/changelog'

    node_modules/@xrow-public/changelog/src/changelog.ts
  else
    echo "Set GITLAB_TOKEN to receive changelog"
    echo 'Hint: is this a feature branch?'
    touch "${dir}/changelog.md"
  fi
  return 0
}

function ci_markdown_parse_env {
  local dir="${1:-.}"
  CI_DOCUMENTATION_LATEST_TAG=$(git tag --list | sort -V | tail -n1)
  export CI_DOCUMENTATION_LATEST_TAG
  echo "[info] Parsing env vars for markdown:"
  for file in $(find "${dir}" -maxdepth 3 -regextype posix-extended -not -path "*/templates/*" -iregex '.*[.]md' | sort -g); do 
    ci_envsubst "${file}"
  done
}

# Build documentation using 'mkdocs build' command
# Parameters:
# 1. The directory containing the documentation source markdown files.
function ci_mkdocs_build {
  local dir="${1:-.}"

  if [[ ! -f ${dir}/mkdocs.yml ]]; then
    echo "site_name: unknown" > ${dir}/mkdocs.yml
  fi

  cat ${TOOLS_DIR}/scripts/files/components/mkdocs/Containerfile > Containerfile
  ci_markdown_parse_env ${dir}
  ci_mkdocs_changelog ${dir}

  cat <(echo "docs_dir: ${dir}") ${dir}/mkdocs.yml > mkdocs.yml
  echo "[info] Building documentation from source in ${dir}."

  mkdocs --version
  mkdocs build --strict --verbose --site-dir release/documentation

  echo "[info] Finished building documentation."
}

function ci_marp_export() {
  mkdir -p release/presentation && chmod 777 release/presentation
  FILES=$( find . -not -path './release*' -name '*.marp.md' -type f )
  for i in $FILES; do
      local dir=$(dirname $i)
      local releasedir="./release/presentation$( echo $dir | sed -e 's/\.\/docs\/presentation//' )"
      echo "Exporting dir $dir to $releasedir";
      mkdir -p $releasedir && chmod 777 $releasedir || true
      cp -R $dir/* $releasedir
      local name=$(basename --suffix=.marp.md $i)
      podman run --rm --privileged -e MARP_USER="$(id -u):$(id -g)" -v $releasedir:/home/marp/app -e LANG=de docker.io/marpteam/marp-cli $name.marp.md --allow-local-files --pptx --output /home/marp/app/$name.pptx
      podman run --rm --privileged -e MARP_USER="$(id -u):$(id -g)" -v $releasedir:/home/marp/app -e LANG=de docker.io/marpteam/marp-cli $name.marp.md --allow-local-files --output /home/marp/app/$name.html
      podman run --rm --privileged -e MARP_USER="$(id -u):$(id -g)" -v $releasedir:/home/marp/app -e LANG=de docker.io/marpteam/marp-cli $name.marp.md --allow-local-files --output /home/marp/app/$name.pdf
  done
}
