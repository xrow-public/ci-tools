#!/bin/bash

function gitpod_connect_kubernetes() {
  eval $(command gp env -e)
  ci_kubernetes_config
  sudo mkdir -p /root/.kube
  sudo cp $HOME/.kube/config /root/.kube/config
  sudo chmod 600 /root/.kube/config
  local namespace="${KUBERNETES_NAMESPACE_OVERWRITE:-$KUBERNETES_NAMESPACE}"
  echo "NAMESPACE: $namespace"
  sudo kubefwd svc -l "app.kubernetes.io/name in ( cache, mysql, sessionservice, rabbitmq, smtp, solr, varnish )" -n $namespace
}
function gitpod_set_env() {
  eval $(command gp env -e)
  ci_kubernetes_config
  rm -Rf .env.local
  rm -Rf .env.dev.local
  rm -Rf .env.prod.local
  touch .env.local
  touch .env.dev.local
  touch .env.prod.local
  local gitpodfile="$HOME/.bashrc.d/gitpod"
  rm -Rf $gitpodfile
  mkdir -p $HOME/.bashrc.d
  printf 'export PATH="%s:%s:%s:$PATH"\n' "$(pwd)/bin" "$(yarn global bin)" "$(yarn bin)" >> $gitpodfile
  printf 'export APP_ENV="%s"\n' "dev" >> $gitpodfile
  local namespace="${KUBERNETES_NAMESPACE_OVERWRITE:-$KUBERNETES_NAMESPACE}"

  local release=$(kubectl get app -n $namespace -o yaml | yq -r '.items[] | select(.spec.chart.metadata.name =="ibexa") | .metadata.name')
  kubectl get cm/${release}-ibexa -n $namespace -o json | \
  jq -r '.data | del(.APP_DEBUG?,.CACHE_POOL?,.APP_ENV?,.CACHE_NAMESPACE?,.CACHE_DSN?,.SESSION_HANDLER?,.SESSION_HANDLER_ID?,.SESSION_PATH?,.LOG_PATH?,.SESSION_SAVE_PATH?)' |\
  jq -r 'del(.HTTPD_START_SERVERS?,.HTTPD_START_SERVERS?)' |\
  jq -r 'keys[] as $k | "\($k)=\"\(.[$k] | .)\""' >> \
  .env.local

  kubectl get cm/${release}-ibexa -n $namespace -o json | \
  jq -r '.data | del(.APP_DEBUG?,.CACHE_POOL?,.APP_ENV?,.CACHE_NAMESPACE?,.CACHE_DSN?,.SESSION_HANDLER?,.SESSION_HANDLER_ID?,.SESSION_PATH?,.LOG_PATH?,.SESSION_SAVE_PATH?)' |\
  jq -r 'del(.HTTPD_START_SERVERS?,.HTTPD_START_SERVERS?)' |\
  jq -r 'keys[] as $k | "export \($k)=\"\(.[$k] | .)\""' >> \
  $gitpodfile

  sed -i "s/\.$namespace\.svc\.cluster\.local\.//" .env.local
  sed -i "s/\.$namespace\.svc\.cluster\.local//" .env.local
  sed -i "s/\.$namespace\.svc\.cluster\.local\.//" $gitpodfile
  sed -i "s/\.$namespace\.svc\.cluster\.local//" $gitpodfile

  printf 'APP_DEBUG=%s\n' "1" >> .env.dev.local
  composer config --global github-oauth.github.com $(printf '%s\n' host=github.com | gp credential-helper get | sort | head -2 | tail -1 | sed 's;password=;;')
  if [ -n "$INSTALLATION_ID" ]; then
    echo "$(date +[%T];) Composer: InstallationID $INSTALLATION_ID"
    composer config --global http-basic.updates.ibexa.co $INSTALLATION_ID $LICENCE_KEY
  fi
  if [ -n "$GITHUB_KEY" ]; then
    echo "$(date +[%T];) Composer: GITHUB_KEY"
    composer config --global github-oauth.github.com $GITHUB_KEY
  fi
  if [ -n "$GITLAB_TOKEN" ]; then
    echo "$(date +[%T];) Composer: GITLAB_TOKEN"
    composer config --global gitlab-token.gitlab.com $GITLAB_TOKEN
  fi
}
function gitpod_s2i_assemble() {
  eval $(command gp env -e)
  gitpod_set_env
  sudo -E /usr/libexec/s2i/assemble
}
function gitpod_s2i_run() {
  eval $(command gp env -e)
  gitpod_set_env
  sudo chmod 777 -R /opt/app-root/etc
  sudo chmod 777 -R /var/log/php-fpm
  sudo rm -Rf /opt/app-root/src
  sudo ln -s $(pwd) /opt/app-root/src
  sudo setfacl -dR -m u:root:rwX -m u:$(whoami):rwX var
  sudo setfacl -R -m u:root:rwX -m u:$(whoami):rwX var
  sudo -E bash /usr/libexec/s2i/run
}