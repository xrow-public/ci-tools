#!/bin/bash

# Run Cypress tests
# Parameters:
# - Path to the test scripts
# - Docker image to use
# - Browser to emulate
# - Space-separated browser options
# Environment variables:
# - CI_ENVIRONMENT_HOST - name of host running review environment
# - CYPRESS_RECORD_KEY - key for recording run in Cypress Cloud (see https://docs.cypress.io/cloud/account-management/projects#Identification)
function ci_cypress_test() {
    local dir="${1:-./tests/e2e}"
    local image="${2:-docker.io/cypress/included:latest}"
    local browser="${3:-firefox}"
    local options="${4:---headless}"

    if [[ -n "$CYPRESS_RECORD_KEY" ]]; then
        options+=" --record"
    fi

    if [[ -v CYPRESS_BASE_URL ]]; then
      echo "[error] Use CI_ENVIRONMENT_URL($CI_ENVIRONMENT_URL) in cypress.config.ts because it will be always overwritten."
      exit 1
    fi

    if [[ -v CI_ENVIRONMENT_URL ]]; then
      echo "[info] CI_ENVIRONMENT_URL = '${CI_ENVIRONMENT_URL}'."
      else
      echo "[info] CI_ENVIRONMENT_URL is not defined."
    fi

    ci_registry_token_login
    export NODE_EXTRA_CA_CERTS
    NODE_EXTRA_CA_CERTS=$(cd "${dir}" || exit 1;find . -name "*.crt" -type f -printf "/e2e/%P" -quit)
    echo "$NODE_EXTRA_CA_CERTS"
    env | grep CI >> /tmp/env.cypress
    env | grep CYPRESS >> /tmp/env.cypress
    env | grep NODE >> /tmp/env.cypress
    $CONTAINER_RUNTIME run -t --env-file /tmp/env.cypress -v "${dir}:/e2e:Z" -w /e2e --entrypoint="" "${image}" bash -c "npm install && cypress run --browser $browser $options"
    return $?
}
