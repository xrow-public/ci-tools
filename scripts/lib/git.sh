#!/bin/bash

if [[ -x "$(command -v git)" ]]; then
  echo "[info] Init global git settings"
  git config --global --add safe.directory '*'
  if [[ -v GITLAB_USER_EMAIL ]]; then
    git config --global user.email "${GITLAB_USER_EMAIL}"
    git config --global user.name "${GITLAB_USER_NAME}"
  fi
fi

function ci_git_last_tag() {
  git fetch --unshallow &> /dev/null || true
  version=$(git describe --tags --abbrev=0)
  echo "${version}"
  return 0 
}

function ci_git_empty_commit() {
  local message="${1:-chore: Automatic empty commit}"
  git fetch --unshallow &> /dev/null || true
  git checkout "${CI_DEFAULT_BRANCH}"
  git remote set-url origin "git@${CI_SERVER_SHELL_SSH_HOST}:${CI_PROJECT_PATH}.git"
  git commit --allow-empty -m "${message}"
  git push origin
}

# Mirror the current git repository.
function ci_git_push() {
  ci_ssh_init

  if [[ ! -x "$(command -v git)" ]]; then
    echo "[info] 'git' command not found, install it."
    dnf install -y git
  fi

  local dst="${1:-$CI_MIRROR_URL}"
  echo "[info] Mirror git repository to '$dst'"
  if [[ -z "$dst" ]]; then
    echo "[error] you need to set one parameter (CI_MIRROR_URL) to call ci_git_push"
    exit 1
  fi
  #git config receive.denyDeleteCurrent false

  git fetch --all
  git branch -r | grep -v '\->' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" | while read remote; do git branch --track "${remote#origin/}" "$remote"; done
  git remote update

  if [[ -v CI_DEFAULT_BRANCH ]]; then
    git checkout $CI_DEFAULT_BRANCH
    git symbolic-ref HEAD refs/heads/$CI_DEFAULT_BRANCH
  fi

  git pull --all
  git clone --bare . /tmp/repo
  GIT_DIR="/tmp/repo"

  if [[ -v CI_DEFAULT_BRANCH ]]; then
    git checkout $CI_DEFAULT_BRANCH
    git symbolic-ref HEAD refs/heads/$CI_DEFAULT_BRANCH
  fi

  git push --mirror $dst

  #if [[ -v GITLAB_USER_EMAIL ]]; then
  #fi

  rm -Rf /tmp/repo
}
