#!/bin/bash

# Start SSH Agent and configure SSH connections to various servers.
function ci_ssh_agent_start() {
  if [[ ! -x "$(command -v ssh-agent)" ]]; then
    echo "[info] 'ssh-agent' command not found, install 'openssh-clients' which provides it."
    dnf install -y openssh-clients
  fi

  if [[ ! -v SSH_AGENT_PID ]]; then
    echo "[info] Start SSH Agent."
    eval $(ssh-agent -s)

    echo "[info] Configure SSH connections to various servers."
    mkdir -p ${HOME}/.ssh
    echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ${HOME}/.ssh/config
    mkdir -p ${HOME}/.ssh
    chmod 700 ${HOME}/.ssh
    touch ${HOME}/.ssh/known_hosts
    chmod 600 ${HOME}/.ssh/known_hosts
    ssh-keyscan github.com || true >> ${HOME}/.ssh/known_hosts
    ssh-keyscan gitlab.com || true >> ${HOME}/.ssh/known_hosts
    ssh-keyscan ssh.dev.azure.com || true >> ${HOME}/.ssh/known_hosts

    if [[ -v CI_SERVER_HOST && "$CI_SERVER_HOST" != "gitlab.com" ]]; then
      echo "[info] Gather SSH public keys for $CI_SERVER_HOST."
      ssh-keyscan $CI_SERVER_HOST || true >> ${HOME}/.ssh/known_hosts
    fi

    echo "[info] Finished configuring SSH connections."
  fi
}

# Add keys from environment variables to the SSH Agent.
function ci_ssh_init() {
  if [ -v GIT_SSH_KEY ]; then
    echo "[info] Environment variable GIT_SSH_KEY exists. Add it to SSH Agent."
    ci_ssh_add_key $GIT_SSH_KEY
  fi

  if [ -v SSH_KEY ]; then
    echo "[info] Environment variable SSH_KEY exists. Add it to SSH Agent."
    ci_ssh_add_key $SSH_KEY
  fi

  if [ -v SSH_PRIVATE_KEY ]; then
    echo "[info] Environment variable SSH_PRIVATE_KEY exists. Add it to SSH Agent."
    ci_ssh_add_key $SSH_PRIVATE_KEY
  fi
}

# Ensure the SSH Agent is running and add a key.
function ci_ssh_add_key() {
  ci_ssh_agent_start
  if [[ -z "$1" ]]; then
    echo "[warning] Environment variable is empty, skip adding to SSH Agent."
    return 0
  fi

  if [[ -f "$1" ]]; then
    echo "[info] Found file '$1' containing SSH key."
    cp "$1" /tmp/$(basename $1)
    sed -i -e '$a\' /tmp/$(basename $1)
    chmod 600 /tmp/$(basename $1)
    ssh-add "/tmp/$(basename $1)"
    return 0
  fi

  local key=$(openssl enc -base64 -d <<< $1)
  if [[ ! -z "$key" ]]; then
    echo "[info] Value is base64-encoded. Add the decoded value to SSH Agent."
    echo "$key" | ssh-add - || true
  else
    echo "[info] Add the raw value to SSH Agent."
    echo "$1" | ssh-add - || true
  fi
}
