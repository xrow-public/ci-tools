#!/bin/bash

function ci_review_template() {
    if [[ ! -v CI_DEPLOY_PASSWORD ]]; then
      echo "Variable CI_DEPLOY_PASSWORD is needed."
      exit 1
    fi;
    if [[ ! -v CI_DEPLOY_USER ]]; then
      echo "Variable CI_DEPLOY_USER is needed."
      exit 1
    fi;
    envsubst < $TOOLS_DIR/scripts/files/review.yaml > /tmp/review.yaml
}

function ci_review_start() {
    export NAMESPACE="review-${CI_PIPELINE_ID}"
    export RELEASE="review-${CI_PIPELINE_ID}"
    if [[ ! -v CI_ENVIRONMENT_URL ]]; then
        export CI_ENVIRONMENT_URL="https://${RELEASE}.${CI_REVIEW_DOMAIN}"
    fi
    ci_kubernetes_connect_local
    ci_kubernetes_cleanup
    ci_review_template
    cat /tmp/review.yaml
    echo "Applying template review.yaml..."
    # update verbose level if required (0-9)
    kubectl apply -f /tmp/review.yaml --v=6

    echo "Review app namespace: $NAMESPACE"
    echo "Review app release: $RELEASE"
    echo "Review is being installed: ${CI_ENVIRONMENT_URL}"
    echo "This may take 15 minutes"
    echo "Check if ibexa/${RELEASE} is deployed"
    kubectl wait --for=condition=Deployed --timeout=15m -n ${NAMESPACE} ibexa/${RELEASE} || return 1
    ci_util_check_url ${CI_ENVIRONMENT_URL} 60 || return 1
    echo "Review app started: ${CI_ENVIRONMENT_URL}"
    return 0
}

function ci_review_stop() {
    ci_kubernetes_connect_local
    ci_review_template
    kubectl delete -f /tmp/review.yaml
    echo "Review app stopped"
}

ci_util_check_url() {
    local url="$1"
    local seconds="$2"
    local start_time="$(date +%s)"
    local end_time=$((start_time + $seconds))
    local current_time
    echo "Check if $url is reachable for $seconds seconds"
    while true; do
        current_time="$(date +%s)"
        status_code=$(curl -s -o /dev/null -w "%{http_code}" $url)
        if [[ "$current_time" -gt "$end_time" ]]; then
          if [[ $status_code -eq 200 ]]; then
            echo "URL $url was available after $seconds seconds."
            return 0
          else
            return 1
          fi
        fi
        if [[ $status_code -ne 200 ]]; then
            echo "Error: URL $url returned status code $status_code"
        fi
        sleep 5
    done
}

