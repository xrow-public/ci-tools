#!/bin/bash


function ci_env_require() {
  local name=${1}
  if [[ ! -v "${name}" ]]; then
    echo "[error] ENV ${name} is required."
    ci_exit_or_return
  fi
}

function ci_dotenv() {
  local file=${ENV_FILE:-.env.local}
  if [[ -f ".env" ]]; then
    echo "[info] Loading env from .env" 
    set -o allexport # enable all variable definitions to be exported
    source <(sed -e "s/\r//" -e "s/'/'\\\''/g" .env)
    set +o allexport
  fi
  if [[ -f "${file}" ]]; then 
    echo "[info] Loading env from ${file}" 
    set -o allexport # enable all variable definitions to be exported
    source <(sed -e "s/\r//" -e "s/'/'\\\''/g" "${file}")
    set +o allexport
  fi
}