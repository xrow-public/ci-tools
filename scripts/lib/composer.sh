#!/bin/bash

export COMPOSER_ALLOW_SUPERUSER="1"
export COMPOSER_EXECUTABLE="/usr/bin/composer"
export COMPOSER_MEMORY_LIMIT="-1"
export COMPOSER_PROCESS_TIMEOUT="${COMPOSER_PROCESS_TIMEOUT:-600}"

if [[ -x "$(command -v composer)" ]]; then
  composer config --global --no-plugins process-timeout ${COMPOSER_PROCESS_TIMEOUT}
fi

function ci_composer_init() {
  if [ ! -x "$(command -v composer)" ]; then
    echo "Found COMPOSER_VERSION, installing dependencies using composer.phar... "

    # Install Composer
    TEMPFILE=$(mktemp)
    RETRIES=6
    for ((i=0; i<$RETRIES; i++)); do

      if [ ! -v COMPOSER_INSTALLER ]; then
        export COMPOSER_INSTALLER="https://getcomposer.org/installer"
      fi

      echo "Downloading $COMPOSER_INSTALLER, attempt $((i+1))/$RETRIES"
      curl -o $TEMPFILE $COMPOSER_INSTALLER && break
      sleep 10
    done
    if [[ $i == $RETRIES ]]; then
      echo "Download failed, giving up."
      exit 1
    fi

    if [ ! -v COMPOSER_VERSION ]; then
      echo "By default, latest composer will be used. If you want to use v1 please use the environment variable COMPOSER_VERSION=1"
      php <$TEMPFILE
    else
      echo "You set the COMPOSER_VERSION"
      php <$TEMPFILE -- --$COMPOSER_VERSION
    fi
    mv composer.phar /usr/local/bin/composer
    export COMPOSER_EXECUTABLE=/usr/local/bin/composer
  else
    export COMPOSER_EXECUTABLE=/usr/bin/composer
  fi
}

composer_auth(){
  rm -Rf ~/.composer/auth.json
  if [[ -v INSTALLATION_ID ]]; then
    echo "$(date +[%T];) Composer: InstallationID $INSTALLATION_ID"
    $COMPOSER_EXECUTABLE config -g http-basic.updates.ibexa.co $INSTALLATION_ID $LICENCE_KEY
  fi
  if [[ -v GITHUB_KEY ]]; then
    echo "$(date +[%T];) Composer: GITHUB_KEY"
    $COMPOSER_EXECUTABLE config -g github-oauth.github.com $GITHUB_KEY
  fi
  if [[ -v GITLAB_TOKEN ]]; then
    echo "$(date +[%T];) Composer: GITLAB_TOKEN"
    $COMPOSER_EXECUTABLE config -g gitlab-token.$CI_SERVER_HOST gitlab-ci-token $GITLAB_TOKEN
  fi
  if [[ -v AUTH_JSON ]]; then
    echo "$(date +[%T];) Composer: AUTH_JSON"
    echo $AUTH_JSON > auth.json
  fi
}
