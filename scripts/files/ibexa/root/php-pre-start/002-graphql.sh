#!/bin/bash

set -e

# @TODO this crashes if the database is not installed.
# php bin/console dbal:run-sql "SELECT 1"
# php bin/console ibexa:graphql:generate-schema
# php bin/console cache:clear
# php bin/console cache:warmup

DIR="config/graphql/types/ezplatform"

if [[ ! -d "$DIR" || "$(ls -A $DIR)" == "" ]]; then
    echo "Running bin/console ibexa:graphql:generate-schema"
    bin/console ibexa:graphql:generate-schema || true
    bin/console cache:clear || true
else
    echo "bin/console ibexa:graphql:generate-schema has run"
fi