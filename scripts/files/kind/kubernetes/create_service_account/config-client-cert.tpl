apiVersion: v1
kind: Config
clusters:
- cluster:
    certificate-authority-data: ${CLUSTER_CA}
    server: ${CLUSTER_ENDPOINT}
  name: ${CLUSTER_NAME}
users:
- name: ${USER}
contexts:
- context:
    cluster: ${CLUSTER_NAME}
    user: ${USER}
    namespace: ${NAMESPACE}
  name: ${CURRENT_CONTEXT}
current-context: ${CURRENT_CONTEXT}