apiVersion: v1
kind: Config
clusters:
- cluster:
    insecure-skip-tls-verify: true
    server: ${CLUSTER_ENDPOINT}
  name: ${CLUSTER_NAME}
users:
- name: ${USER}
  user:
    token: ${TOKEN}
contexts:
- context:
    cluster: ${CLUSTER_NAME}
    user: ${USER}
    namespace: ${NAMESPACE}
  name: ${CURRENT_CONTEXT}
current-context: ${CURRENT_CONTEXT}