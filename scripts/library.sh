#!/bin/bash

if [[ -z "$BASH" ]]; then echo "[error] Please run this script $0 with bash"; exit; fi

# Load the library in your script with the following commands:
# Loads the library from the specified version. If no version is specified, the main branch is used.
# [root@localhost]# source <(curl -s -k https://gitlab.com/xrow-public/ci-tools/-/raw/main/scripts/library.sh) [version]
# [root@localhost]# CI_TOOLS_VERSION=0.0.0; source <(curl -s -k https://gitlab.com/xrow-public/ci-tools/-/raw/main/scripts/library.sh)

if [[ ! -x "$(command -v tar)" ]]; then
  dnf install -y tar
fi

if [[ -x "$(command -v dnf)" && $CI_CONFIG_FLAG_FASTESTMIRROR = "true" ]]; then
  dnf config-manager --save --setopt=fastestmirror=True
fi

if [[ -v 1 ]]; then
  export CI_TOOLS_VERSION="${1}"
else
  export CI_TOOLS_VERSION="${CI_TOOLS_VERSION:-main}"
fi

export CI_SERVER_HOST="${CI_SERVER_HOST:-gitlab.com}"
export CI_TOOLS_DOWNLOAD="${CI_TOOLS_DOWNLOAD:-https://${CI_SERVER_HOST}/xrow-public/ci-tools/-/archive/${CI_TOOLS_VERSION}/ci-tools-${CI_TOOLS_VERSION}.tar.gz}"

# Check if the library is already available on the os
if [[ -f scripts/library.sh ]]; then
  export TOOLS_DIR="$(pwd)"
  echo "[info] Using ci-tools from ${TOOLS_DIR}"
elif [[ -f /scm/ci-tools/scripts/library.sh ]]; then
  export TOOLS_DIR="/scm/ci-tools"
  echo "[info] Using ci-tools from ${TOOLS_DIR}"
else
  echo "[info] Loading ci-tools version '${CI_TOOLS_VERSION}' from ${CI_TOOLS_DOWNLOAD}"
  export TOOLS_DIR=$(mktemp -d -p /tmp)
  curl -s -k ${CI_TOOLS_DOWNLOAD} | tar -xz --strip-components=1 -C $TOOLS_DIR
fi
source $TOOLS_DIR/scripts/lib/ci.sh

ci_init