# CI Tools

![pipeline status](https://gitlab.com/xrow-public/ci-tools/badges/main/pipeline.svg) ![Latest Release](https://gitlab.com/xrow-public/ci-tools/-/badges/release.svg)


This catalog brings components to enterprises to build flexible pipelines for continuous delivery to GitLab. Those pipelines can be used for applications and infrastructure. This model be applicable to teams of developers and operators that ship together.

## Design Principles

The catalog was designed under the following 7 principles:

* Prioritize **continuous delivery** over continuous deployment. [Read](https://www.atlassian.com/continuous-delivery/principles/continuous-integration-vs-delivery-vs-deployment)
* **Infrastructure** is delivered with continuous delivery **the same way** as applications.
* Every web application is running on **Kubernetes** which should be the current choice for Enterprises.
* Web applications in a container shall use **technology abstraction** like [S2I](https://catalog.redhat.com/search?gs&q=S2I) from Red Hat as a stable base.
* Containers shipped via **HELM** packages to Kubernetes.
* The underlying automation stack is compatible to **other automation platforms** such as GitHub, Azure DevOps, … .
* **Support and consult** any enterprises following this pattern.
* Stay opinionated and open for feedback.

## Documentation

[Pipeline documentation](https://ci-tools.xrow.de/)

Sample application pipeline:

![](https://gitlab.com/xrow-public/ci-tools/-/raw/main/docs/images/pipeline.png)

The following documentation can be constantly extended with information like:

* How to build pipelines
* How to use pipelines
* Pipeline tests (e.g. lint, code tests…)
* …
